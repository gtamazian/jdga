#!/usr/bin/env python3
"""Routines to parse an NCBI Contamination Report file."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import itertools
from typing import List, NamedTuple, Tuple


class Excluded(NamedTuple):
    """An excluded sequence record."""
    name: str
    length: int
    source: str


Span = Tuple[int, int]


class Trimmed(NamedTuple):
    """A trimmed sequence record."""
    name: str
    length: int
    span: List[Span]
    source: str


class Duplicate(NamedTuple):
    """A duplicated sequence record."""
    names: List[str]
    length: int


class Report(NamedTuple):
    """Contents of the report."""
    excluded: List[Excluded]
    trimmed: List[Trimmed]
    duplicated: List[Duplicate]


def read(path: str) -> Report:
    """Read an NCBI Contamination Report file.

    :note: The function produces a side effect by reading from file
      `path`.

    :path: The NCBI Contamination Report file name.

    :returns: The parted contamination report with sections of
      excluded, trimmed, and duplicated sequences.

    """

    def parse_excluded(line: str) -> Excluded:
        """Helper function to parse an excluded sequence line."""
        name, length, source = str.split(str.rstrip(line), None, 2)
        return Excluded(name, int(length), source)

    def parse_range(line: str) -> Span:
        """Helper function to parse a single span (range)."""
        parts = str.split(line, '..', 1)
        return (int(parts[0]), int(parts[1]))

    def parse_spans(line: str) -> List[Span]:
        """Helper function to parse spans of a trimmed sequence."""
        return [parse_range(k) for k in str.split(line, ',')]

    def parse_trimmed(line: str) -> Trimmed:
        """Helper function to parse a trimmed sequence line."""
        name, length, span, source = str.split(str.rstrip(line), None, 3)
        return Trimmed(name, int(length), parse_spans(span), source)

    def parse_duplicate(line: str) -> Duplicate:
        """Helper function to parse a duplicated sequence line."""
        sep = str.index(line, '(')
        prefix, suffix = str.split(line[:sep]), line[sep:]
        names = [str.split(k, '|', 1)[1] for k in prefix]
        length = int(str.split(str.strip(suffix, '()'), ' ')[0])
        return Duplicate(names, length)

    def read_excluded(lines: List[str]) -> List[Excluded]:
        """Helper function to read the excluded sequence section."""
        return [
            parse_excluded(k) for k in itertools.islice(
                itertools.takewhile(
                    lambda x: x != '',
                    itertools.dropwhile(
                        lambda x: not str.startswith(x, 'Exclude:'), lines)),
                2, None)
        ]

    def read_trimmed(lines: List[str]) -> List[Trimmed]:
        """Helper function to read the trimmed sequence section."""
        return [
            parse_trimmed(k) for k in itertools.islice(
                itertools.takewhile(
                    lambda x: x != '',
                    itertools.dropwhile(
                        lambda x: not str.startswith(x, 'Trim:'), lines)), 2,
                None)
        ]

    def read_duplicates(lines: List[str]) -> List[Duplicate]:
        """Helper function to read the duplicate sequence section."""
        return [
            parse_duplicate(k) for k in itertools.islice(
                itertools.takewhile(
                    lambda x: x != '',
                    itertools.dropwhile(
                        lambda x: not str.startswith(x, 'Duplicated:'),
                        lines)), 2, None)
        ]

    with open(path) as report_file:
        lines = [str.rstrip(k) for k in report_file]

    return Report(read_excluded(lines), read_trimmed(lines),
                  read_duplicates(lines))
