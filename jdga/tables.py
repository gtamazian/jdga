#!/usr/bin/env python3
"""Routines to produce tables for the paper."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import collections
import functools
import operator
from typing import Counter, List, Optional, Set, Tuple, TypeVar

from jdga import assembly
from jdga import busco
from jdga import common
from jdga import fasta
from jdga import gff3
from jdga import intergenome
from jdga import orthodb
from jdga import sequence
from jdga import vcf


def print_busco_table(config: intergenome.BuscoConfig) -> None:
    """Print the table of annotated BUSCO genes.

    :note: The function produces the same side effects as function
      :py:func:`jdga.intergenome.load_gene_anchors` and also prints
      text to the standard output.

    :param config: The BUSCO dataset configuration as for function
      :py:func:`jdga.intergenome.load_gene_anchors`.

    """
    def region_to_string(region: intergenome.Region) -> str:
        """Helper function for printing a BUSCO region."""
        return str.format('{:s}\t{:d}\t{:d}\t{:s}', region[0], region[1],
                          region[2], region[3])

    chrom_names = dict(gff3.chromosome_seqids(gff3.read(config.gff3_path)))

    print(
        str.join('\t', [
            'BLOCK_NUM', 'CAT_CHROM', 'CAT_SEQID', 'CAT_START', 'CAT_END',
            'CAT_GENE', 'ALN_SEQID', 'ALN_START', 'ALN_END', 'ALN_GENE',
            'ALN_ANC_NUM', 'BLOCK_TYPE'
        ]))

    for k, block in enumerate(intergenome.split_into_blocks(
            intergenome.assign_numbers(intergenome.load_gene_anchors(config))),
                              start=1):
        for ref_gene, aln_gene, anc_num in block:
            print(
                str.format('BLOCK_{:d}\t{:s}\t{:s}\t{:s}\t{:d}\t{:s}', k,
                           dict.get(chrom_names, ref_gene[0], 'NA'),
                           region_to_string(ref_gene),
                           region_to_string(aln_gene), anc_num + 1,
                           'single' if len(block) == 1 else 'multiple'))


def print_assembly_summary(path: str, excluded_scaffolds: Set[bytes]) -> None:
    """Print sequence summary for assembly from a FASTA file.

    :note: The function produces side effects by calling function
      :py:func:`jdga.fasta.read` to read sequences from a FASTA file
      and by printing text to the standard output.

    :param path: The assembly FASTA file name.

    :param excluded_scaffolds: Scaffolds to exclude from the assembly summary.

    """
    def print_seq_summary(sequences: List[bytes], label: str) -> None:
        """Helper function to be applied to scaffolds and contigs."""
        seq_lengths = [len(k) for k in sequences]
        n50, l50 = assembly.n_l_values(seq_lengths, 50)
        n90, l90 = assembly.n_l_values(seq_lengths, 90)
        print(
            str.format(
                'Total {:s} length:\t{:d} bp\n'
                'Number of {:s}s:\t{:d}\n'
                'Length of longest {:s}:\t{:d} bp\n'
                '{:s} N50:\t{:d} bp\n'
                '{:s} L50:\t{:d} {:s}s\n'
                '{:s} N90:\t{:d} bp\n'
                '{:s} L90:\t{:d} {:s}s', label, sum(seq_lengths), label,
                len(seq_lengths), label, max(seq_lengths),
                str.capitalize(label), n50, str.capitalize(label), l50, label,
                str.capitalize(label), n90, str.capitalize(label), l90, label))

    def print_gap_summary(sequences: List[bytes]) -> None:
        """Helper function to print gap statistics."""
        total_seq_length = sum(len(k) for k in sequences)
        gap_sizes = [
            k[1] - k[0] for seq in sequences for k in sequence.gap_regions(seq)
        ]
        print(
            str.format(
                'Total length of gaps:\t{:d} bp\n'
                'Number of gaps:\t{:d}\n'
                'Gap percentage:\t{:.2f}%', sum(gap_sizes), len(gap_sizes),
                sum(gap_sizes) / total_seq_length * 100))

    records = [
        fasta.remove_comment(fasta.remove_soft_mask(k))
        for k in fasta.read(path)
    ]
    scaffolds = [k.seq for k in records if k.name not in excluded_scaffolds]
    print_seq_summary(scaffolds, 'scaffold')
    print_seq_summary(assembly.contigs(scaffolds), 'contig')
    print_gap_summary(scaffolds)
    print(
        str.format('GC content:\t{:.2f}%',
                   float(assembly.gc_content(scaffolds) * 100)))


def print_busco_summary(busco_path: str, orthodb_root: str, level_id: str,
                        species_id: str) -> None:
    """Print the summary of BUSCO genes for the specified reference species.

    :note: The function produces side effects by calling functions
      :py:func:`jdga.busco.read_full_table` and
      :py:func:`jdga.orthodb.load_og_ncbi_geneids` and by printing to
      the standard output.

    :param busco_path: The BUSCO full table TSV file.

    :param orthodb_root: The OrthoDB dump root directory.

    :param level_id: OrthoDB level ID that specifies orthologous
      groups (OGs) to be returned.  The level ID is fixed for a BUSCO
      full table TSV file.

    :param species_id: The taxonomy ID as passed to the function
      returned by :py:func:`jdga.orthodb.og_ncbi_ids`.  The ID
      specifies a reference species in the produced table.

    """
    T = TypeVar('T')  # pylint: disable=invalid-name

    def deduplicate(values: List[T]) -> List[T]:
        """Helper function to remove duplicates from a list."""
        return list(set(values))

    busco_all_genes = deduplicate([(k.busco_id, k.status)
                                   for k in busco.read_full_table(busco_path)])

    Busco = Tuple[str, str]

    def filter_busco_genes() -> List[Busco]:
        """Helper function to filter the BUSCO genes for a species."""
        species_genes = set(k[0] for k in orthodb.load_og_ncbi_geneids(
            orthodb_root, level_id, species_id))
        return [k for k in busco_all_genes if k[0] in species_genes]

    busco_cat_genes = filter_busco_genes()

    labels = ['Complete', 'Duplicated', 'Fragmented', 'Missing']

    def prepare_columns(busco_genes: List[Busco]) -> List[Tuple[int, float]]:
        """Helper function to prepare a pair of columns for the table."""
        def arrange_values(counter: Counter[str]) -> List[int]:
            """Helper function to arrange BUSCO categories in proper order."""
            return [dict.get(counter, k, 0) for k in labels]

        def add_percentages(values: List[int]) -> List[Tuple[int, float]]:
            """Helper function to add percentage values to counts."""
            return [(k, 100 * k / sum(values)) for k in values]

        return add_percentages(
            arrange_values(collections.Counter(k[1] for k in busco_genes)))

    print(
        str.join('\t',
                 ['Group', 'All count', 'All perc', 'Cat count', 'Cat perc']))
    for k in zip(labels, prepare_columns(busco_all_genes),
                 prepare_columns(busco_cat_genes)):
        print(
            str.format('{:s}\t{:d}\t{:.2f}\t{:d}\t{:.2f}', k[0], k[1][0],
                       k[1][1], k[2][0], k[2][1]))


def variant_type(variant: vcf.record.Record) -> Optional[str]:
    """Return FreeBayes's INFO/TYPE value of a variant.

    :param variant: A parsed VCF variant record.

    :returns: The `INFO/TYPE` value of the variant.

    """
    return common.find_opt_alist(
        [common.list_to_pair(str.split(k, '=', 1)) for k in variant.info],
        'TYPE')


def single_sample(variant: vcf.record.Record) -> bool:
    """Check that a variant contains genotype for a single sample.

    :param variant: A parsed VCF variant record.

    :returns: Does `variant` contain a genotype for a single sample?

    """
    return (variant.genotypes is not None and len(variant.genotypes.fields) > 0
            and variant.genotypes.fields[0] == 'GT'
            and len(variant.genotypes.samples) == 1)


def single_gt_to_str(variant: vcf.record.Record) -> str:
    """Convert a genotype of a single sample to a string.

    :param variant: A parsed VCF variant record.

    :returns: The string representing a single sample genotype or 'NA'
      if `variant` contains no genotypes or genotypes of multiple
      samples.

    """
    if variant.genotypes is None:
        return 'NA'

    if not variant.genotypes.fields or variant.genotypes.fields[0] != 'GT':
        return 'NA'

    if len(variant.genotypes.samples) > 1:
        return 'NA'

    return variant.genotypes.samples[0][0]


def print_variant_loci_summary(vcf_path: str) -> None:
    """Print the table of variant loci summaries.

    The loci summary includes the variant sequence (CHROM), the number
    of alternative alleles, the variant type as returned by FreeBayes
    (INFO/TYPE), and the single sample genotype.  The genotype value
    has three possible values: '0/1', '1/1', and 'other'.

    :note: The function produces side effects by calling function
      :py:func:`jdga.vcf.open_vcf_file`, iterating contents of the
      opened VCF file, and printing to the standard output.

    :param vcf_path: A single-sample VCF file by FreeBayes.

    """
    def gt_to_str(genotype: str) -> str:
        """Helper function to convert a GT value to a string."""
        if genotype in {'0/0', '0/1', '1/1', '0', '1'}:
            return genotype

        return 'other'

    def get_output_line(variant: vcf.record.Record) -> str:
        """Helper function to form a line for the output table."""
        var_type = variant_type(variant)
        return str.format(
            '{:s}\t{:d}\t{:s}\t{:s}\t{:d}\t{:s}\t{:s}', variant.chrom,
            variant.pos,
            variant.ref, 'NA' if len(variant.alt) > 1 else variant.alt[0],
            len(variant.alt), 'NA' if var_type is None else var_type,
            'NA' if variant.genotypes is None else gt_to_str(
                variant.genotypes.samples[0][0]))

    with open(vcf_path) as vcf_file:
        _, _, data = vcf.open_vcf_file(vcf_file)
        for variant in data:
            assert single_sample(variant)
            print(get_output_line(variant))


def print_variant_effects(vcf_path: str) -> None:
    """Print the table of variant loci summaries.

    The printed table includes the variant type as returned by
    FreeBayes and the variant effect as predicted by SnpEff.

    :note: The function produces side effects by calling function
      :py:func:`jdga.vcf.open_vcf_file`, iterating contents of the
      opened VCF file, and printing to the standard output.

    :param vcf_path: A single-sample VCF file produced by FreeBayes
      and annotated by SnpEff.

    """
    def get_ann(variant: vcf.record.Record) -> Optional[str]:
        """Helper function to extract SnpEff's ANN field."""
        return common.find_opt_alist(
            [common.list_to_pair(str.split(k, '=', 1)) for k in variant.info],
            'ANN')

    def effects(ann: str) -> List[str]:
        """Helper function to extract effect names from SnpEff's ANN field."""
        return [str.split(k, '|', 2)[1] for k in str.split(ann, ',')]

    def variant_effects_to_str(variant: vcf.record.Record) -> List[str]:
        """Helper function that forms effect records."""
        ann = get_ann(variant)
        if ann is None:
            return []

        var_type = variant_type(variant)

        return [
            str.format('{:s}\t{:s}\t{:d}\t{:s}', k, var_type, len(variant.alt),
                       single_gt_to_str(variant)) for k in effects(ann)
        ]

    with open(vcf_path) as snpeff_vcf_file:
        _, _, data = vcf.open_vcf_file(snpeff_vcf_file)
        for variant in data:
            assert single_sample(variant)
            for line in variant_effects_to_str(variant):
                print(line)


def print_base_counts(path: str, excluded: Set[bytes]) -> None:
    """Print the table of base pair counts in sequences from a FASTA file.

    :note: The functions produces side effects by calling function
      :py:func:`jdga.fasta.read` and by printing to the standard output.

    :param path: The name of a FASTA file with nucleotide sequences.

    :param excluded: Names of sequences to be excluded from counting
      base pairs.

    """
    seqs = [
        fasta.remove_comment(fasta.remove_soft_mask(k))
        for k in fasta.read(path)
    ]
    counts = functools.reduce(operator.add,
                              (collections.Counter(k.seq)
                               for k in seqs if k.name not in excluded))
    for base, count in sorted(dict.items(counts), key=operator.itemgetter(0)):
        print(str.format('{:s}\t{:d}', chr(base), count))
