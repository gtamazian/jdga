#!/usr/bin/env python3
"""Functions for processing genome assemblies."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import functools
from fractions import Fraction
from typing import List, Tuple

from jdga import sequence as seq


def gap_lengths(sequences: List[bytes]) -> List[int]:
    """Get the list of gap lengths in sequences.

    :param records: Nucleotide sequences.

    :returns: Lengths of gaps in the sequences.

    """
    return [
        pos_e - pos_s for k in sequences for pos_s, pos_e in seq.gap_regions(k)
    ]


def contigs(sequences: List[bytes]) -> List[bytes]:
    """Extract contigs from sequences.

    :param scaffolds: Nucleotide sequences.

    :returns: Contig sequences.
    """
    return [
        part for k in sequences for part in seq.to_contigs(k)
        if isinstance(part, bytes)
    ]


def gc_content(sequences: List[bytes]) -> Fraction:
    """Get the GC content percentage of sequences.

    :param sequences: Nucleotide sequences.

    :returns: The GC content percentage.

    """
    if not sequences:
        return Fraction(0)

    Acc = Tuple[int, int]

    def len_without_gaps(sequence: bytes) -> int:
        """Return the length of a sequence without gaps (Ns)."""
        return len(sequence) - bytes.count(sequence, b'N')

    def gc_bases(sequence: bytes) -> int:
        """Count the number of G and C base pairs in a sequence."""
        return bytes.count(sequence, b'C') + bytes.count(sequence, b'G')

    def process_sequence(acc: Acc, new_sequence: bytes) -> Acc:
        """Helper function passed to functools.reduce."""
        gc_count, total_length = acc
        return (gc_count + gc_bases(new_sequence),
                total_length + len_without_gaps(new_sequence))

    init: Acc = (gc_bases(sequences[0]), len_without_gaps(sequences[0]))
    gc_count, total_length = functools.reduce(process_sequence, sequences[1:],
                                              init)
    return Fraction(gc_count, total_length)


def n_l_values(seq_lengths: List[int], percentage: int) -> Tuple[int, int]:
    """Get the N and L values for sequence lengths.

    :param seq_lengths: Sequence lengths.

    :param percentage: The percentage for computing the N and L values
      (e.g., `percentage` equal to 50 will lead to computing the N50
      and L50 values). Value of `percentage` must be greater or equal
      to 0 and less than 100.

    :returns: The N and L values for the specified percentage.

    """
    if not seq_lengths:
        return (0, 0)

    assert 0 <= percentage < 100

    total_length = sum(seq_lengths)
    threshold = total_length * (100 - percentage) / 100
    sorted_lengths = sorted(seq_lengths, reverse=True)

    remainder = total_length
    k = -1
    while remainder >= threshold:
        k += 1
        remainder -= sorted_lengths[k]

    return (sorted_lengths[k], k + 1)
