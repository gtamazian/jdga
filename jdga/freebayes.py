#!/usr/bin/env python3
"""Routines for launching FreeBayes in parallel."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import concurrent.futures
import itertools
import subprocess
from typing import Callable, Iterator, List, NamedTuple, Optional, Set

from jdga import fasta
from jdga import sequence


def qual(line: bytes) -> Optional[float]:
    """Get the variant quality (QUAL) of a VCF variant record.

    :param line: A VCF variant record line.

    :returns: The variant quality (QUAL) value.

    """
    qual_field = bytes.split(line, b'\t', 6)[5]
    return None if qual_field == b'.' else float(qual_field)


def filter_by_qual(output: bytes, min_qual: float) -> List[bytes]:
    """Filter FreeBayes output by minimal variant quality.

    The function splits the output into lines and removes variant
    lines which quality (QUAL) is less than `min_qual`.  Variants with
    missing quality are removed.

    :param output: FreeBayes output.

    :param min_qual: The minimal variant quality value.

    :returns: VCF lines from `output` which belong to a VCF header or
      denote variants with quality greater or equal to `min_qual`.

    """
    def pass_qual(value: Optional[float]) -> bool:
        """Helper function for filtering the list of VCF lines."""
        if value is None:
            return False
        return value >= min_qual

    return [
        k for k in bytes.splitlines(output)
        if bytes.startswith(k, b'#') or pass_qual(qual(k))
    ]


def launch_freebayes(ref_path: str, bam_paths: List[str], params: List[str],
                     min_qual: float) -> List[bytes]:
    """Launch FreeBayes with specified input data and parameters.

    :note: The function produces a side effect by launching an
      external program and capturing its output.

    :param ref_path: The FASTA file of a reference genome.

    :param bam_paths: The BAM files of read alignments.

    :param params: Command-line parameters to be passed to FreeBayes.

    :param min_qual: The minimal variant quality (QUAL) value.

    :returns: FreeBayes output in the VCF format.

    """
    run = subprocess.run(['freebayes', '-f', ref_path] + bam_paths + params,
                         stderr=subprocess.DEVNULL,
                         stdout=subprocess.PIPE,
                         check=True)
    return filter_by_qual(run.stdout, min_qual)


class Chunk(NamedTuple):
    """Parameters for launching FreeBayes on a part of a genome."""
    seq: bytes  #: a sequence name
    start: int  #: the start position in a sequence
    end: int  #: the end position in a sequence
    ploidy: int  #: sequence ploidy (usually 1 or 2)


Launcher = Callable[[Chunk], List[bytes]]


def get_freebayes_launcher(ref_path: str, bam_paths: List[str],
                           params: List[str], min_qual: float) -> Launcher:
    """Create a function that launches FreeBayes for a specific sequence.

    Reference genome and read alignment files are fixed in the
    returned function.

    :param ref_path: The FASTA file of a reference genome.

    :param bam_path: The BAM files of read alignments.

    :param params: Command-line parameters to be passed to FreeBayes.

    :param min_qual: The minimal variant quality value.

    :returns: The function that accepts a sequence name, start and end
      positions of a region in the sequence, and the sequence ploidy
      and launches FreeBayes to call variants in the sequence region.

    """
    def launcher(chunk: Chunk) -> List[bytes]:
        """Function to be returned."""
        return launch_freebayes(ref_path, bam_paths, [
            '-p',
            str.format('{:d}', chunk.ploidy), '-r',
            str.format('{:s}:{:d}-{:d}', bytes.decode(chunk.seq), chunk.start,
                       chunk.end)
        ] + params, min_qual)

    return launcher


class GenotypingTask(NamedTuple):
    """Parameters for parallel variant calling using FreeBayes."""
    ref_path: str  #: The FASTA file of a reference genome.
    bam_paths: List[str]  #: The BAM files of read alignments.
    min_gap: int  #: The minimal length of gaps at which the sequence is split.
    haploid: List[bytes]  #: Haploid sequences of the reference genome.
    params: List[str]  #: Command-line parameters to be passed to FreeBayes.
    min_qual: float  #: The variant quality (QUAL) threshold.


def chunks(records: List[fasta.Record], min_gap: int,
           haploid: Set[bytes]) -> List[Chunk]:
    """Split records into chunks by gaps in their sequences.

    The record sequences are split into parts as in function
    :py:func:`sequence.regions_by_gaps`.

    :param records: Nucleotide sequence records as returned by
      function :py:func:`fasta.read`.

    :param min_gap: The minimal length of gaps which form sequence
      parts.

    :param haploid: Names of sequences which variants have haploid
      genotypes.

    :returns: Regions of the sequence chunks.

    """
    # for better performance, convert haploid to a set
    return [
        Chunk(k.name, region[0], region[1], 1 if k.name in haploid else 2)
        for k in records
        for region in sequence.regions_by_gaps(k.seq, min_gap)
    ]


def parallel_freebayes(task: GenotypingTask,
                       num_workers: int) -> List[List[bytes]]:
    """Launch FreeBayes in parallel.

    The function launches FreeBayes to call variants for the specified
    reference genome and read alignments using `num_workers` parallel
    threads.  The reference genome sequences are split into regions by
    gaps as in function :py:func:`sequence.regions_by_gaps` and
    variant calling for each region is launched in a separate thread.
    For sequences from `task.haploid`, FreeBayes is launched in the
    haploid variant calling mode.

    :note: The function produces side effects by launching FreeBayes
      in parallel.

    :param task: The set of parameters for variant calling.

    :param num_workers: The number of parallel FreeBayes threads.

    :returns: Output from each launched FreeBayes process.

    """
    launcher_fn = get_freebayes_launcher(task.ref_path, task.bam_paths,
                                         task.params, task.min_qual)

    reference = [
        fasta.remove_comment(fasta.remove_soft_mask(k))
        for k in fasta.read(task.ref_path)
    ]

    with concurrent.futures.ThreadPoolExecutor(
            max_workers=num_workers) as executor:
        tasks = executor.map(
            launcher_fn, chunks(reference, task.min_gap, set(task.haploid)))
        return list(tasks)


def write_vcf(task_output: List[List[bytes]], path: str) -> None:
    """Write output from parallel FreeBayes runs to a VCF file.

    :note: The function produces a side effect by writing to file
      `path`.

    :param task_output: Output from parallel FreeBayes runs as
      returned by :py:func:`parallel_freebayes`.

    :param path: Output VCF file name.

    """
    if not task_output:
        return

    with open(path, 'wb') as vcf_file:

        def is_header(line: bytes) -> bool:
            return bytes.startswith(line, b'#')

        def write_lines(lines: Iterator[bytes]) -> None:
            """Helper function that writes lines to the VCF file."""
            vcf_file.writelines(k + b'\n' for k in lines)

        # write the VCF header without the command line tag
        write_lines(k for k in itertools.takewhile(is_header, task_output[0])
                    if not bytes.startswith(k, b'##commandline'))

        # write output from the first run with its header
        for k in task_output:
            write_lines(
                itertools.dropwhile(lambda x: bytes.startswith(x, b'#'), k))
