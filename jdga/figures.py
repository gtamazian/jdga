#!/usr/bin/env python3
"""Routines to produce figures for the paper."""

__author__ = "Gaik Tamazian"
__copyright__ = "Copyright 2021, Gaik Tamazian"
__email__ = "mail@gtamazian.com"
__license__ = "GPLv3"

import functools
import math
from typing import Dict, List, NamedTuple, Optional, Set, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import patches

from jdga import colors
from jdga import common
from jdga import fasta
from jdga import gff3
from jdga import intergenome
from jdga import sequence

_CENTROMERE_GAP_LENGTHS = [2_000_000, 3_000_000]


def centromere(seq: bytes, gap_lengths: Set[int]) -> Optional[Tuple[int, int]]:
    """Get a putative centromere region based on gaps in a sequence.

    :param seq: A nucleotide sequence.

    :param gap_lengths: Lengths of gaps that may represent a centromere.

    :returns: The region of a gap which length is in `gap_lengths` if
      `seq` does not contain any other gaps which lengths are in
      `gap_lengths`, otherwise the function returns `None.`
    """
    putative_centromeres = [
        k for k in sequence.gap_regions(seq) if k[1] - k[0] in gap_lengths
    ]
    return putative_centromeres[0] if len(putative_centromeres) == 1 else None


Region = Tuple[int, int]


class ChromosomeBlock(NamedTuple):
    """Chromosome-like structure for a genome-based figure."""

    seqid: str
    name: str
    length: int
    centromere: Optional[Region]
    stretches: List[Tuple[int, int, bool]] = []


def describe_genome(
    seq_records: List[fasta.Record], gff3_records: List[gff3.Record]
) -> List[ChromosomeBlock]:
    """Extract information about a genome from NCBI FASTA and GFF3 records.

    :param seq_records: Sequence records from an NCBI FASTA file.

    :param gff3_records: Records from an NCBI GFF3 file.

    :returns: Description of chromosomes for producing a genome-based figure.

    """

    def get_chromosomes() -> List[Tuple[str, Tuple[str, int]]]:
        """Extract chromosome information from `gff3_records`."""
        return [
            (k.seqid, (k.attributes["Name"], k.end))
            for k in gff3_records
            if gff3.is_chromosome(k)
        ]

    def get_centromeres() -> List[Tuple[str, Tuple[int, int]]]:
        """Get centromere regions from NCBI GFF3 records.

        Coordinates of the returned regions are zero-based and
        half-open as in the BED format.

        """
        return [
            (k.seqid, (k.start - 1, k.end))
            for k in gff3_records
            if k.feature_type == "centromere"
        ]

    centromeres = get_centromeres()

    if centromeres:
        # Centromere regions are given in the GFF3 records, so there
        # is no need to scan sequences.
        return [
            ChromosomeBlock(seqid, name, length, cent_region)
            for seqid, ((name, length), cent_region) in common.combine_alists(
                get_chromosomes(), centromeres
            )
        ]

    seq_dict = {bytes.decode(name): seq for name, seq in seq_records}

    return [
        ChromosomeBlock(
            seqid,
            name,
            length,
            centromere(seq_dict[seqid], set(_CENTROMERE_GAP_LENGTHS)),
        )
        for seqid, (name, length) in get_chromosomes()
    ]


class PlotSettings(NamedTuple):
    """Settings for a chromosome plot."""

    height: int = 20
    x_border: int = 5_000_000
    delta: int = 100_000
    min_width: int = 3_000_000
    tip_width: int = 2_000_000
    base_hatch: str = "////"
    alpha: float = math.pi / 6
    interblock_dist: int = 500_000


def draw_chromosome(
    axes: plt.Axes,
    block: ChromosomeBlock,
    x_start: float,
    y_start: float,
    settings: PlotSettings,
) -> None:
    """Helper function that draws a chromosome on the plot.

    :note: The function produces a side effect by modifying `axes`.

    :param axes: The Matplotlib Axes object to draw a chromosome to.

    :param block: Structure that describe the chromosome to plot.

    :param x_start: Starting position of the chromosome on X-axis.

    :param y_start: Starting position of the chromosome on Y-axis.

    :param settings: Chromosome plot settings.

    """
    plt.Axes.text(
        axes,
        0,
        y_start + settings.height * 7 / 6,
        block.name,
        horizontalalignment="right",
        verticalalignment="center",
    )

    # draw stretched regions first
    for position, _, invisible in block.stretches:
        if not invisible:
            plt.Axes.add_patch(
                axes,
                patches.Rectangle(
                    (x_start + position, y_start),
                    width=settings.min_width,
                    height=settings.height,
                    hatch="/" * 12,
                    fill=False,
                    edgecolor="grey",
                    linewidth=0.5,
                ),
            )

    if block.centromere is not None:
        plt.Axes.add_patch(
            axes,
            patches.Rectangle(
                (x_start, y_start),
                width=block.centromere[0],
                height=settings.height,
                fill=None,
                linewidth=0.5,
            ),
        )
        plt.Axes.add_patch(
            axes,
            patches.Rectangle(
                (x_start + block.centromere[1], y_start),
                width=block.length - block.centromere[1],
                height=settings.height,
                fill=None,
                linewidth=0.5,
            ),
        )
        cent_start = x_start + block.centromere[0]
        cent_width = block.centromere[1] - block.centromere[0]

        plt.Axes.add_patch(
            axes,
            patches.Polygon(
                np.array(
                    [
                        [cent_start, y_start + settings.height],
                        [cent_start + settings.delta, y_start + settings.height],
                        [cent_start + cent_width / 2, y_start + settings.height / 2],
                        [cent_start + settings.delta, y_start],
                        [cent_start, y_start],
                    ]
                ),
                fill=True,
                color="black",
                linewidth=0.5,
            ),
        )

        plt.Axes.add_patch(
            axes,
            patches.Polygon(
                np.array(
                    [
                        [cent_start + cent_width / 2, y_start + settings.height / 2],
                        [
                            cent_start + cent_width - settings.delta,
                            y_start + settings.height,
                        ],
                        [cent_start + cent_width, y_start + settings.height],
                        [cent_start + cent_width, y_start],
                        [cent_start + cent_width - settings.delta, y_start],
                    ]
                ),
                fill=True,
                color="black",
                linewidth=0.5,
            ),
        )
    else:
        plt.Axes.add_patch(
            axes,
            patches.Rectangle(
                (x_start, y_start),
                width=block.length,
                height=settings.height,
                fill=None,
                linewidth=0.5,
            ),
        )


def initialize_plot(
    genome: List[ChromosomeBlock], settings: PlotSettings
) -> Tuple[plt.Figure, plt.Axes]:
    """Initialize a genome plot using provided chromosomes and settings.

    :param genome: Genome description as returned by function
      :py:func:`describe_genome`.

    :param settings: Plot settings.

    :returns: The 2-tuple of the Matplotlib figure and axes objects.

    """
    fig, axes = plt.subplots()
    plt.Axes.axis(axes, "off")
    plt.Axes.set_xlim(axes, 0, 2 * settings.x_border + max(k.length for k in genome))
    plt.Axes.set_ylim(axes, 0, 3 * settings.height * len(genome))

    return (fig, axes)


def plot_busco_blocks(
    genome: List[ChromosomeBlock],
    blocks: List[intergenome.Region],
    settings: PlotSettings,
) -> plt.Figure:
    """Plot continuous blocks formed by BUSCO genes.

    :param genome: Description of genome chromosomes as returned by
      function :py:func:`describe_genome`.

    :param blocks: Multianchor blocks on the reference genome
      described by `genome` as returned by function
      :py:func:`jdga.intergenome.block_boundaries`.

    :param settings: Plot settings.

    :returns: The Matplotlib figure object that visualizes how the
      blocks are located on the reference genome chromosomes.

    """
    fig, axes = initialize_plot(genome, settings)
    y_chrom = 0

    def draw_block(block: intergenome.Region, x_start: float, y_start: float) -> None:
        """Helper function that draws a block."""
        y_block = y_start + 4 / 3 * settings.height
        _, start, end, _ = block
        plt.Axes.add_patch(
            axes,
            patches.Rectangle(
                (x_start + start, y_block), width=end - start, height=settings.height
            ),
        )
        plt.Axes.add_patch(
            axes,
            patches.Rectangle(
                (x_start + start, y_block),
                width=max(end - start, settings.delta),
                height=settings.height,
                fill=None,
                linewidth=0.5,
            ),
        )

    for chrom in reversed(genome):
        draw_chromosome(axes, chrom, settings.x_border, y_chrom, settings)
        for current_block in blocks:
            if current_block[0] == chrom.seqid:
                draw_block(current_block, settings.x_border, y_chrom)
        y_chrom += 3 * settings.height

    return fig


class SyntenyBlock(NamedTuple):
    """An oriented synteny block on a reference genome."""

    ref_seq: str
    aln_seq: str
    start: int
    end: int
    reverse: bool
    single_gene: bool
    seq_start: bool
    seq_end: bool


def add_stretch(
    chrom: ChromosomeBlock,
    start: int,
    delta: int,
    invisible: bool,
    settings: PlotSettings,
) -> ChromosomeBlock:
    """Helper function to add a stretched region to a chromosome."""
    new_centromere: Optional[Region]
    if chrom.centromere is not None:
        if start < chrom.centromere[0]:
            delta += max(start + settings.min_width - chrom.centromere[0], 0)
            new_centromere = (chrom.centromere[0] + delta, chrom.centromere[1] + delta)
        else:
            new_centromere = chrom.centromere
    else:
        new_centromere = None

    return ChromosomeBlock(
        chrom.seqid,
        chrom.name,
        chrom.length + delta,
        new_centromere,
        chrom.stretches + [(start, delta, invisible)],
    )


class PlotEnvironment(NamedTuple):
    """Axes, plot settings, and chromosome styles."""

    axes: plt.Axes
    settings: PlotSettings
    chrom_styles: Dict[str, colors.FillStyle]


def draw_regular_block(
    block: SyntenyBlock,
    start: Tuple[float, float],
    stretched_chrom: ChromosomeBlock,
    prev_end: Optional[float],
    env: PlotEnvironment,
) -> Tuple[ChromosomeBlock, float]:
    """Helper function that draws a synteny block."""
    offset = functools.reduce(
        lambda length, stretch: length + stretch[1], stretched_chrom.stretches, 0
    )

    if prev_end is not None:
        if block.start + offset - prev_end < env.settings.interblock_dist:
            stretched_chrom = add_stretch(
                stretched_chrom,
                block.start + offset,
                env.settings.interblock_dist,
                True,
                env.settings,
            )
            offset += env.settings.interblock_dist

    x_start, y_start = start

    x_block = x_start + block.start + offset
    y_block = y_start + 4 / 3 * env.settings.height

    width = block.end - block.start
    if width < env.settings.min_width:
        stretched_chrom = add_stretch(
            stretched_chrom,
            block.start + offset,
            env.settings.min_width - width,
            False,
            env.settings,
        )
        width = env.settings.min_width

    style = dict.get(env.chrom_styles, block.aln_seq, colors.FillStyle((0, 0, 0), 0))

    def block_points() -> np.ndarray:
        """Helper function to get points of a synteny block."""
        epsilon = math.tan(env.settings.alpha) * env.settings.tip_width
        if block.reverse:
            points = [
                (x_block, y_block + env.settings.height / 2),
                (x_block + epsilon, y_block),
                (x_block + width, y_block),
                (x_block + width, y_block + env.settings.height),
                (x_block + epsilon, y_block + env.settings.height),
            ]
        else:
            points = [
                (x_block, y_block),
                (x_block + width - epsilon, y_block),
                (x_block + width, y_block + env.settings.height / 2),
                (x_block + width - epsilon, y_block + env.settings.height),
                (x_block, y_block + env.settings.height),
            ]

        return np.array(points)

    def block_start() -> np.ndarray:
        """Helper function to get points of the sequence end mark."""
        epsilon = math.tan(env.settings.alpha) * env.settings.tip_width
        if block.reverse:
            x_end = x_block + width
            points = [
                (x_end - epsilon, y_block),
                (x_end, y_block),
                (x_end, y_block + env.settings.height),
                (x_end - epsilon, y_block + env.settings.height),
            ]
        else:
            points = [
                (x_block, y_block),
                (x_block + epsilon, y_block),
                (x_block + epsilon, y_block + env.settings.height),
                (x_block, y_block + env.settings.height),
            ]

        return np.array(points)

    def block_end() -> np.ndarray:
        """Helper function to get points of the sequence start mark."""
        epsilon = math.tan(env.settings.alpha) * env.settings.tip_width
        if block.reverse:
            points = [
                (x_block, y_block + env.settings.height / 2),
                (x_block + epsilon, y_block),
                (x_block + 2 * epsilon, y_block),
                (x_block + epsilon, y_block + env.settings.height / 2),
                (x_block + 2 * epsilon, y_block + env.settings.height),
                (x_block + epsilon, y_block + env.settings.height),
            ]
        else:
            x_end = x_block + width
            points = [
                (x_end - 2 * epsilon, y_block),
                (x_end - epsilon, y_block),
                (x_end, y_block + env.settings.height / 2),
                (x_end - epsilon, y_block + env.settings.height),
                (x_end - 2 * epsilon, y_block + env.settings.height),
                (x_end - epsilon, y_block + env.settings.height / 2),
            ]

        return np.array(points)

    Color = Union[colors.RGB, str]

    def draw_contour(
        points: np.ndarray,
        color: Optional[Color],
        hatch: Optional[str],
        edgecolor: Optional[str],
        linewidth: float,
    ) -> None:
        """Helper function to draw a synteny block figure."""
        plt.Axes.add_patch(
            env.axes,
            patches.Polygon(
                points,
                color=color,
                fill=color is not None,
                hatch=hatch,
                edgecolor=edgecolor,
                linewidth=linewidth,
            ),
        )

    def draw_circle(
        center: Tuple[float, float],
        color: Optional[Color],
        hatch: Optional[str],
        edgecolor: Optional[str],
        linewidth: float,
        width: float = env.settings.min_width,
        height: float = env.settings.height * 3 / 4,
    ) -> None:
        """Helper function to draw a single-gene synteny block figure."""
        plt.Axes.add_patch(
            env.axes,
            patches.Ellipse(
                center,
                width,
                height,
                color=color,
                fill=color is not None,
                hatch=hatch,
                edgecolor=edgecolor,
                linewidth=linewidth,
            ),
        )

    def circle_center():
        """Get the circle center coordinates."""
        return (x_block + env.settings.min_width / 2, y_block + env.settings.height / 2)

    if block.single_gene:
        draw_circle(
            circle_center(),
            color=style.color,
            hatch=None,
            edgecolor=None,
            linewidth=0.0,
        )
    else:
        draw_contour(
            block_points(), color=style.color, hatch=None, edgecolor=None, linewidth=0.0
        )

    if style.hatch > 0:
        if block.single_gene:
            draw_circle(
                circle_center(),
                color=None,
                hatch=(env.settings.base_hatch * style.hatch),
                edgecolor="white" if colors.is_dark(style.color) else "black",
                linewidth=0.5,
            )
        else:
            draw_contour(
                block_points(),
                color=None,
                hatch=(env.settings.base_hatch * style.hatch),
                edgecolor="white" if colors.is_dark(style.color) else "black",
                linewidth=0.5,
            )

    if block.seq_start:
        if block.single_gene:
            draw_circle(
                circle_center(),
                color=None,
                hatch=None,
                edgecolor="black",
                linewidth=1.5,
            )
        else:
            draw_contour(
                block_start(), color="black", hatch=None, edgecolor=None, linewidth=0.0
            )

    if block.seq_end:
        if block.single_gene:
            draw_circle(
                circle_center(),
                height=env.settings.height / 3,
                width=env.settings.min_width / 3,
                color="black",
                hatch=None,
                edgecolor=None,
                linewidth=0.0,
            )
        else:
            draw_contour(
                block_end(), color="black", hatch=None, edgecolor=None, linewidth=0.0
            )

    if block.single_gene:
        draw_circle(
            circle_center(), color=None, hatch=None, edgecolor="black", linewidth=0.5
        )
    else:
        draw_contour(
            block_points(), color=None, hatch=None, edgecolor="black", linewidth=0.5
        )

    return stretched_chrom, x_block + width - x_start


def plot_synteny_blocks(
    ref_genome: List[ChromosomeBlock],
    aln_genome: List[ChromosomeBlock],
    blocks: List[SyntenyBlock],
    settings: PlotSettings,
) -> plt.Figure:
    """Plot synteny blocks on chromosomes of a reference genome.

    :param ref_genome: Reference genome chromosomes as returned by
      function :py:func:`describe_genome`.

    :param aln_genome: Chromosomes of an aligned genome as returned by
      function :py:func:`describe_genome`.

    :param blocks: Synteny blocks formed by sequences of the aligned
      genome on sequences of the reference genome.

    :param settings: Parameters that specify the plot.

    :returns: The Matplotlib figure object that shows synteny blocks
      arranged on the reference genome sequences.

    """
    fig, axes = initialize_plot(ref_genome, settings)
    y_chrom = 0
    chrom_styles = dict(colors.fill_styles([k.seqid for k in aln_genome]))

    max_chrom = max(k.length for k in ref_genome)

    for chrom in reversed(ref_genome):
        # first we draw blocks and determine stretches on the chromosome
        stretched_chrom = chrom
        prev_end = None
        for current_block in blocks:
            if current_block.ref_seq == chrom.seqid:
                stretched_chrom, prev_end = draw_regular_block(
                    current_block,
                    (settings.x_border, y_chrom),
                    stretched_chrom,
                    prev_end,
                    PlotEnvironment(axes, settings, chrom_styles),
                )
        draw_chromosome(axes, stretched_chrom, settings.x_border, y_chrom, settings)
        y_chrom += 3 * settings.height
        max_chrom = max(max_chrom, stretched_chrom.length)

    plt.Axes.set_xlim(axes, 0, 2 * settings.x_border + max_chrom)

    return fig


class BreakpointMark(NamedTuple):
    """Breakpoint mark for the breakpoint graph."""

    seq: str
    start: int
    end: int


def plot_breakpoints(
    ref_genome: List[ChromosomeBlock],
    breakpoints: List[BreakpointMark],
    settings: PlotSettings,
) -> plt.Figure:
    """Plot breakpoints on chromosomes of a reference genome.

    :param ref_genome: Same as for function :py:func:`plot_synteny_blocks`.

    :param breakpoints: List of breakpoint marks on the reference genome chromosomes.

    :param settings: Same as for function :py:func:`plot_synteny_blocks`.

    """
    fig, axes = initialize_plot(ref_genome, settings)
    y_chrom = 0

    def draw_mark(
        mark: BreakpointMark,
        x_start: float,
        y_start: float,
        stretched_chrom: ChromosomeBlock,
        prev_end: Optional[float],
    ) -> Tuple[ChromosomeBlock, float]:
        """Helper function that draws a breakpoint mark."""
        offset = functools.reduce(
            lambda length, stretch: length + stretch[1], stretched_chrom.stretches, 0
        )

        if prev_end is not None:
            if mark.start + offset - prev_end < settings.interblock_dist:
                stretched_chrom = add_stretch(
                    stretched_chrom,
                    mark.start + offset,
                    settings.interblock_dist,
                    True,
                    settings,
                )
                offset += settings.interblock_dist

        x_mark = x_start + mark.start + offset
        y_mark = y_start + 4 / 3 * settings.height

        width = mark.end - mark.start

        if width < settings.min_width:
            stretched_chrom = add_stretch(
                stretched_chrom,
                mark.start + offset,
                settings.min_width - width,
                False,
                settings,
            )
            width = settings.min_width

        def block_points() -> np.ndarray:
            """Helper function to get points of a breakpoint mark."""
            return np.array(
                [
                    (x_mark, y_mark + settings.height),
                    (x_mark + width, y_mark + settings.height),
                    (x_mark + width / 2, y_mark),
                ]
            )

        def draw_contour(
            points: np.ndarray,
            color: Optional[str],
            edgecolor: Optional[str],
            linewidth: float,
        ) -> None:
            """Helper function to draw breakpoint mark figure."""
            plt.Axes.add_patch(
                axes,
                patches.Polygon(
                    points,
                    color=color,
                    fill=color is not None,
                    edgecolor=edgecolor,
                    linewidth=linewidth,
                ),
            )

        draw_contour(block_points(), color="red", edgecolor=None, linewidth=0.0)
        draw_contour(block_points(), color=None, edgecolor="black", linewidth=0.5)

        return stretched_chrom, x_mark + width - x_start

    max_chrom = max(k.length for k in ref_genome)

    for chrom in reversed(ref_genome):
        # first we draw blocks and determine stretches on the chromosome
        stretched_chrom = chrom
        prev_end = None
        for current_mark in breakpoints:
            if current_mark.seq == chrom.seqid:
                stretched_chrom, prev_end = draw_mark(
                    current_mark, settings.x_border, y_chrom, stretched_chrom, prev_end
                )
        draw_chromosome(axes, stretched_chrom, settings.x_border, y_chrom, settings)
        y_chrom += 3 * settings.height
        max_chrom = max(max_chrom, stretched_chrom.length)

    plt.Axes.set_xlim(axes, 0, 2 * settings.x_border + max_chrom)

    return fig


def plot_plain_synteny_blocks(
    ref_genome: List[ChromosomeBlock],
    aln_genome: List[ChromosomeBlock],
    blocks: List[SyntenyBlock],
    settings: PlotSettings,
) -> plt.Figure:
    """Plot simplified synteny blocks without direction and end marks."""
    fig, axes = initialize_plot(ref_genome, settings)
    y_chrom = 0
    chrom_styles = dict(colors.fill_styles([k.name for k in aln_genome]))

    def draw_block(
        block: SyntenyBlock,
        x_start: float,
        y_start: float,
        stretched_chrom: ChromosomeBlock,
        prev_end: Optional[float],
    ) -> Tuple[ChromosomeBlock, float]:
        """Helper function that draws a synteny block."""
        offset = functools.reduce(
            lambda length, stretch: length + stretch[1], stretched_chrom.stretches, 0
        )

        if prev_end is not None:
            if block.start + offset - prev_end < settings.interblock_dist:
                stretched_chrom = add_stretch(
                    stretched_chrom,
                    block.start + offset,
                    settings.interblock_dist,
                    True,
                    settings,
                )
                offset += settings.interblock_dist

        x_block = x_start + block.start + offset
        y_block = y_start + 4 / 3 * settings.height

        width = block.end - block.start
        if width < settings.min_width:
            stretched_chrom = add_stretch(
                stretched_chrom,
                block.start + offset,
                settings.min_width - width,
                False,
                settings,
            )
            width = settings.min_width

        style = dict.get(chrom_styles, block.aln_seq, colors.FillStyle((0, 0, 0), 0))

        def block_points() -> np.ndarray:
            """Helper function to get points of a synteny block."""
            return [
                (x_block, y_block + settings.height),
                (x_block, y_block),
                (x_block + width, y_block),
                (x_block + width, y_block + settings.height),
            ]

        Color = Union[colors.RGB, str]

        def draw_contour(
            points: np.ndarray,
            color: Optional[Color],
            hatch: Optional[str],
            edgecolor: Optional[str],
            linewidth: float,
        ) -> None:
            """Helper function to draw a synteny block figure."""
            plt.Axes.add_patch(
                axes,
                patches.Polygon(
                    points,
                    color=color,
                    fill=color is not None,
                    hatch=hatch,
                    edgecolor=edgecolor,
                    linewidth=linewidth,
                ),
            )

        draw_contour(
            block_points(), color=style.color, hatch=None, edgecolor=None, linewidth=0.0
        )

        if style.hatch > 0:
            draw_contour(
                block_points(),
                color=None,
                hatch=(settings.base_hatch * style.hatch),
                edgecolor="white" if colors.is_dark(style.color) else "black",
                linewidth=0.5,
            )

        draw_contour(
            block_points(), color=None, hatch=None, edgecolor="black", linewidth=0.5
        )

        return stretched_chrom, x_block + width - x_start

    max_chrom = max(k.length for k in ref_genome)

    for chrom in reversed(ref_genome):
        # first we draw blocks and determine stretches on the chromosome
        stretched_chrom = chrom
        prev_end = None
        for current_block in blocks:
            if current_block.ref_seq == chrom.name:
                stretched_chrom, prev_end = draw_block(
                    current_block, settings.x_border, y_chrom, stretched_chrom, prev_end
                )
        draw_chromosome(axes, stretched_chrom, settings.x_border, y_chrom, settings)
        y_chrom += 3 * settings.height
        max_chrom = max(max_chrom, stretched_chrom.length)

    plt.Axes.set_xlim(axes, 0, 2 * settings.x_border + max_chrom)

    return fig
