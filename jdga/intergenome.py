#!/usr/bin/env python3
"""Routines for marker-based genome comparison."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import itertools
import functools
import operator
from typing import List, NamedTuple, Tuple

from jdga import busco
from jdga import common
from jdga import gff3
from jdga import orthodb


class BuscoConfig(NamedTuple):
    """Data files and options for analyzing BUSCO gene annotation."""
    orthodb_path: str
    level_id: str
    species_id: str
    busco_path: str
    gff3_path: str


Region = Tuple[str, int, int, str]


def load_gene_anchors(config: BuscoConfig) -> List[Tuple[Region, Region]]:
    """Load pairs of gene regions based on BUSCO annotation of a genome.

    :note: The function produces side effects by reading from the
      OrthoDB dump files, the BUSCO full-table TSV file, and the NCBI
      GFF3 file.  The side effects are caused by calling functions
      :py:func:`jdga.busco.read_full_table`,
      :py:func:`jdga.orthodb.load_og_ncbi_geneids`, and
      :py:func:`jdga.gff3.read`.

    :param config: The BUSCO dataset configuration structure that
      contains the root path of the OrthoDB dump, OrthoDB level ID and
      reference species ID, the BUSCO full table path, and the path to
      the NCBI GFF3 annotation file of the reference genome.

    :returns: Pairs which first element is a gene in the reference
      genome and the second element is the homologous gene in the
      BUSCO-annotated genome.

    """
    return [
        (ref_reg, aln_reg + (orthodb_id, ))
        for orthodb_id, (aln_reg, ref_reg) in common.combine_alists(
            busco.single_complete_coords(
                busco.read_full_table(config.busco_path)),
            common.merge_alists(
                orthodb.load_og_ncbi_geneids(
                    config.orthodb_path, config.level_id, config.species_id),
                gff3.gene_coords(gff3.read(config.gff3_path))))
        if ref_reg is not None
    ]


class Anchor(NamedTuple):
    """An anchor present in a pair of genomes."""
    ref: Region
    aln: Region
    number: int


def assign_numbers(markers: List[Tuple[Region, Region]]) -> List[Anchor]:
    """Assign numbers to markers by their positions in the aligned genome.

    :param markers: Markers between a pair of genomes as returned by
      function :py:func:`load_gene_anchors`.

    :returns: The list of markers with added numbers on sequences of
      the aligned genome.

    """
    return [
        Anchor(ref_reg, aln_reg, reg_num)
        for _, aln_anchors in itertools.groupby(sorted(
            (k for k in markers if k[1] is not None), key=lambda x: x[1][:2]),
                                                key=lambda x: x[1][0])
        for reg_num, (ref_reg, aln_reg) in enumerate(aln_anchors)
    ]


def split_into_blocks(anchors: List[Anchor]) -> List[List[Anchor]]:
    """Split anchors into continuous blocks on the reference genome.

    :param anchors: The list of numbered anchors (markers) as returned
      by function :py:func:`assign_numbers`.

    :returns: Continuous blocks of anchors that follow each other on
      the reference genome.

    """
    if not anchors:
        return []

    def adjacent(anchor_a: Anchor, anchor_b: Anchor) -> bool:
        return (anchor_a.aln[0] == anchor_b.aln[0]
                and abs(anchor_a.number - anchor_b.number) == 1)

    Acc = Tuple[List[List[Anchor]], List[Anchor]]

    def process_anchor(acc: Acc, new_anchor: Anchor) -> Acc:
        """Helper function to be passed to functools.reduce."""
        blocks, current = acc
        if adjacent(current[-1], new_anchor):
            list.append(current, new_anchor)
        else:
            list.append(blocks, current)
            current = [new_anchor]
        return (blocks, current)

    sorted_anchors = sorted(anchors, key=operator.attrgetter('ref'))
    init: Acc = ([], [sorted_anchors[0]])
    blocks, last_block = functools.reduce(process_anchor, sorted_anchors[1:],
                                          init)

    if last_block:
        list.append(blocks, last_block)

    return blocks


def block_boundaries(blocks: List[List[Anchor]]) -> List[Region]:
    """Get start and end positions for each multianchor block.

    Blocks that contain a single anchor are excluded.

    :param blocks: Blocks of anchors on a reference genome as returned
      by :py:func:`split_into_blocks`.

    :returns: Start and end positions for each block with names of
      anchors at the beginning and end of the block.

    """
    return [(anchors[0].ref[0], anchors[0].ref[1], anchors[-1].ref[2],
             str.format('{:s}-{:s}', anchors[0].ref[3], anchors[-1].ref[3]))
            for anchors in blocks if len(anchors) > 1]
