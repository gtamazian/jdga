#!/usr/bin/env python3
"""Routines shared between modules of the package."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import List, Optional, Tuple, TypeVar

T = TypeVar('T')  # pylint: disable=invalid-name
S = TypeVar('S')  # pylint: disable=invalid-name
R = TypeVar('R')  # pylint: disable=invalid-name


def combine_alists(
        first: List[Tuple[R, S]],
        second: List[Tuple[R, T]]) -> List[Tuple[R, Tuple[S, Optional[T]]]]:
    """Combine elements from two association lists.

    The resulting list is based on the first list which elements are
    always present in the result.  An element from the second list may
    be missing from the result if no element from the first list has
    the same key.  The function is similar to the left join operation
    in SQL.

    :param first: An association list which elements will be present
      in the result.

    :param second: An association list which elements might be missing
      from the result.

    :returns: The association list with keys from `first` which values
      are tuples of values from `first` and `second`.  If a key was
      missing in `second`, then the second element of the value tuple
      is `None`.

    """
    second_dict = dict(second)
    return [(k, (v, dict.get(second_dict, k))) for k, v in first]


def merge_alists(first: List[Tuple[R, S]],
                 second: List[Tuple[S, T]]) -> List[Tuple[R, T]]:
    """Merge two association lists.

    :param first: An association list which first elements will be
      present in the merged list.

    :param second: An association list which second elements will be
      present in the merged list.

    :returns: The association list which first elements are taken from
      `first` and second elements are taken from `second`.

    """
    second_dict = dict(second)
    return [(j, second_dict[k]) for j, k in first if k in second_dict]


def find_alist(alist: List[Tuple[S, T]], elem: S) -> Optional[T]:
    """Find a value for the specified element in an association list.

    :param alist: An association list.

    :param elem: The element which value is to be found in the list.

    :returns: The value that corresponds to `elem` if `elem` is
      present in `alist`, otherwise `None` is returned.

    """
    for key, value in alist:
        if key == elem:
            return value

    return None


def find_opt_alist(alist: List[Optional[Tuple[S, T]]], elem: S) -> Optional[T]:
    """Find a value in an optional association list.

    This functions acts like :py:func:`find_alist` but allows `None`
    values in `alist`.

    :param alist: An association list which elements may contain `None` values.

    :param elem: The element which value is to be found in the list.

    :returns: The value that corresponds to `elem` if `elem` is
      present in `alist`, otherwise `None` is returned.

    """
    return find_alist([k for k in alist if k is not None], elem)


def list_to_pair(values: List[T]) -> Optional[Tuple[T, T]]:
    """Convert a list of two elements to a 2-tuple.

    :param values: The list to be converted to a tuple.

    :returns: The tuple with elements from the list if the list
      contains two elements, `None` otherwise.

    """
    if len(values) == 2:
        return (values[0], values[1])

    return None
