#!/usr/bin/env python3
"""Functions for processing VCF genotype (GT) records.

Genotype representation
  This submodule uses two data structures to represent genotypes from
  a VCF file.  A genotype without missing alleles is represented by a
  2-tuple which first element is the list of allele numbers and the
  second element is a logical value indicating whether the genotype is
  phased.  A missing genotype is represented by a 2-tuple which first
  element is the genotype ploidy and the second element is a logical
  value indicating phasing.  The phasing status of missing genotypes
  is kept to avoid data loss during a VCF file processing.

Assertions
  Functions that process VCF-format genotype strings
  (:py:func:`separator` and :py:func:`of_string`) assert that the
  strings must not contain both phased and unphased allele separators
  (that is, ``'|'`` and ``'/'``) and that missing and non-missing
  alleles must not be mixed.

"""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import List, NamedTuple, Optional, Union


class Record(NamedTuple):
    """Genotype record with ploidy and phasing status."""
    alleles: Union[List[int], int]
    phased: bool


def separator(genotype_string: str) -> Optional[str]:
    """Return the genotype separator character.

    According to the VCF specification (section 1.6.2 "Genotype
    fields"), alleles of a non-haploid genotype can be separated from
    each other by either ``'|'`` or ``'/'``.  Character ``'|'``
    denotes a phased genotype and character ``'/'`` denotes a
    non-phased one.  For haploid genotypes (e.g., a genotype of a
    variant on the mitochondrial chromosome, chromosome Y, or
    chromosome X of a male sample), the function returns `None`.

    The function asserts that `genotype` must not contain both
    separator characters (e.g., genotype record ``0/1|2`` is
    forbidden).

    :param genotype_string: A genotype string from a VCF file.

    :returns: The allele separator character for `genotype_string` if
      `genotype_string` presents a non-haploid genotype, `None`
      otherwise.

    """
    assert not ('|' in genotype_string and '/' in genotype_string), \
        'genotype strin must not contain both separators'

    if '/' in genotype_string:
        return '/'

    if '|' in genotype_string:
        return '|'

    return None


def of_string(genotype_string: str) -> Record:
    """Parse a genotype record string.

    According to section 1.6.2 "Genotype fields" of the VCF
    specification, ``'.'`` must be specified for each missing allele
    if a variant call could not be made.  Therefore, the function
    asserts that missing and non-missing alleles must not be mixed
    (e.g., genotype record ``./0`` is forbidden).

    :param genotype_string: A genotype string from a VCF file.

    :returns: The genotype record as defined in this submodule.

    """
    allele_sep = separator(genotype_string)
    alleles = str.split(genotype_string, allele_sep)
    phased = allele_sep == '|' if allele_sep is not None else False

    if '.' in alleles:
        assert set(alleles) == {'.'}, \
            'missing and non-missing alleles must not be mixed'
        return Record(len(alleles), phased)

    return Record([int(k) for k in alleles], phased)


def is_missing(genotype: Record) -> bool:
    """Check if a genotype is missing.

    :param genotype: A parsed genotype record.

    :returns: Does `genotype` represent a missing genotype?

    """
    return isinstance(genotype.alleles, int)


def ploidy(genotype: Record) -> int:
    """Return the genotype ploidy.

    :param genotype: A parsed genotype record.

    :returns: The ploidy of `genotype`.

    """
    return genotype.alleles if isinstance(genotype.alleles, int) else len(
        genotype.alleles)


def is_haploid(genotype: Record) -> bool:
    """Check if a genotype is haploid.

    :param genotype: A parsed genotype record.

    :returns: Is `genotype` haploid?

    """
    return ploidy(genotype) == 1


def is_diploid(genotype: Record) -> bool:
    """Check if a genotype is diploid.

    :param genotype: A parsed genotype record.

    :returns: Is `genotype` diploid?

    """
    return ploidy(genotype) == 2


def to_string(genotype: Record) -> str:
    """Convert a genotype to the string according to the VCF format.

    :param genotype: A parsed genotype record.

    :returns: The string representing `genotype` in the VCF format.

    """
    allele_sep = '|' if genotype.phased else '/'

    if isinstance(genotype.alleles, int):
        return str.join(allele_sep, '.' * genotype.alleles)

    return str.join(allele_sep,
                    (str.format('{:d}', k) for k in genotype.alleles))


def is_reference(genotype: Record) -> bool:
    """Check if a genotype does not differ from its reference genome.

    The function returns `False` for a missing genotype.

    :param genotype: A parsed genotype record.

    :returns: Does `genotype` differ from its reference genome?

    """

    # According to the VCF specification (section 1.6.2 "Genotype
    # fields"), zero always indicates the reference allele.

    return False if isinstance(genotype.alleles, int) else all(
        k == 0 for k in genotype.alleles)


def is_heterozygous(genotype: Record) -> bool:
    """Check if a genotype contains different alleles.

    The function returns `False` for a missing genotype.

    :param genotype: A parsed genotype record.

    :returns: Does `genotype` contain different alleles?

    """
    return (False if isinstance(genotype.alleles, int) else
            len(set(genotype.alleles)) > 1)
