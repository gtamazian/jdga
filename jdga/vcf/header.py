#!/usr/bin/env python3
"""Functions for working with a VCF header line."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import List

_COLUMNS = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']


def get_samples(header_line: str) -> List[str]:
    """Return the list of sample IDs from a VCF header line.

    The function returns an empty list if the specified VCF header
    does not contain any sample IDs.

    :param header_line: A VCF header line.
    :return: The list of sample IDs from the header line.

    """
    assert str.startswith(header_line, '#CHROM'), \
        'a VCF header line must start with "#CHROM"'
    return str.split(str.rstrip(header_line), '\t')[9:]


def create(samples: List[str]) -> str:
    """Create a VCF header line with the specified sample IDs.

    If an empty list is given for *samples*, then the function creates
    a header line without the "FORMAT" column and sample IDs.

    :param samples: Sample IDs to be included in the header line.
    :returns: The VCF header line with the specified sample IDs.

    """
    if samples:
        return '#' + str.join('\t', _COLUMNS + ["FORMAT"] + samples)

    return '#' + str.join('\t', _COLUMNS)


def is_header(line: str) -> bool:
    """Is the given line a VCF header line?

    :param line: A line from a VCF file.

    :returns: Is the line a VCF header?

    """
    return str.startswith(line, '#' + str.join('\t', _COLUMNS))
