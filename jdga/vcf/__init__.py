#!/usr/bin/env python3
"""Routines for working with VCF files."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import Iterator, List, NamedTuple, TextIO

from jdga.vcf import header
from jdga.vcf import meta
from jdga.vcf import record


class VariantFile(NamedTuple):
    """Data for reading variants from a VCF file."""
    meta: List[meta.Meta]
    samples: List[str]
    data: Iterator[record.Record]


def open_vcf_file(stream: TextIO) -> VariantFile:
    """Open a VCF file for reading variant records.

    :note: The function produces side effects by reading VCF
      meta-information and header lines from `stream` and returning an
      iterator for reading parsed variants records from `stream`.

    :param stream: A VCF file stream.

    :returns: The named tuple which contains parsed VCF
      meta-information lines, sample names, and the iterator of parsed
      variant records.

    """
    line = ''
    meta_lines: List[meta.Meta] = []
    for line in stream:
        if not meta.is_meta_line(line):
            break
        list.append(meta_lines, meta.of_string(line))

    assert header.is_header(line), \
        'VCF header line must follow meta-information lines'

    return VariantFile(meta_lines, header.get_samples(line),
                       (record.of_string(k) for k in stream))
