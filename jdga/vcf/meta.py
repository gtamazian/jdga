#!/usr/bin/env python3
"""Functions for parsing VCF meta-information lines."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import re
from typing import List, NamedTuple, Optional, Tuple, Union

from jdga import common

MetaValue = Union[str, List[Tuple[str, str]]]

# In functions field_id, field_number, field_type, and to_string I
# check the type of meta.value instead of calling function
# is_structured due to limitations of mypy. The proper way is to use
# `is_structured(meta)` instead of `isinstance(meta.value, list)`.


class Meta(NamedTuple):
    """A meta-information line from the header of a VCF file."""
    key: str
    value: MetaValue


def of_string(line: str) -> Meta:
    """Parse a VCF meta-information line.

    :param line: A meta-information line from a VCF file.

    :returns: The parsed meta-information line.

    """
    assert str.startswith(line, '##'), \
        'a meta-information line must start with "##"'

    # we skip prefix "##" by slicing
    key, value = str.split(str.rstrip(line[2:]), '=', 1)

    def split_structured(value_part: str) -> List[str]:
        """Split values from a structured VCF meta-information line.

        I have to introduce a special function instead of using
        `str.split` because a comma, which serves as a separator
        between the values, might be present within a value.  In this
        function, I skip such commas by passing strings enclosed in
        quotes using a regular expression.

        """
        fields_regexp = re.compile(
            r"""
            [^=,]+       # field name
            =
            (?:"[^"]*")  # field value enclosed in quotes
            |
            (?:[^,]+)    # field value without quotes
            """, re.VERBOSE)
        return re.findall(fields_regexp, value_part)

    def parse_structured(value: str) -> List[Tuple[str, str]]:
        """Parse a structured values of a VCF meta-information line."""

        # This function could be implemented in a single line using
        # the list generator expression, by mypy failed to detect that
        # str.split(k, '=', 1) resulted in a pair of strings that
        # could be converted to the 2-string tuple.  Therefore, I
        # introduced a loop and specified the type of the list which
        # the string tuples were added to in the loop.

        fields: List[Tuple[str, str]] = []
        for k in split_structured(value):
            name, value = str.split(k, '=', 1)
            list.append(fields, (name, value))
        return fields

    # I had to introduce a separate variable with the specified type
    # to avoid confusion caused by mypy.

    parsed_value: MetaValue
    if str.startswith(value, '<') and str.endswith(value, '>'):
        # we skip characters '<' and '>' by slicing
        parsed_value = parse_structured(value[1:-1])
    else:
        parsed_value = value

    return Meta(key, parsed_value)


def is_structured(meta: Meta) -> bool:
    """Check if a meta-information line is structured.

    :param meta: A parsed VCF meta-information line.

    :returns: Does `meta` represent a structured meta-information line?

    """
    return isinstance(meta.value, list)


def field_id(meta: Meta) -> Optional[str]:
    """Return the ID of a structured VCF meta-information line.

    :param meta: A parsed VCF meta-information line.

    :returns: The ID string if `meta` is structured, `None` otherwise.

    """
    return (common.find_alist(meta.value, 'ID') if isinstance(
        meta.value, list) else None)


def field_number(meta: Meta) -> Optional[Union[str, int]]:
    """Return the number field of a structured VCF meta-information line.

    :param meta: A parsed VCF meta-information line.

    :returns: The Number field value if `meta` is structured and
      contains the field of that kind, `None` otherwise.

    """
    def parse_number(
            number_string: Optional[str]) -> Optional[Union[str, int]]:
        """Try to parse a number and return the string if the parsing fails."""
        if number_string is None:
            return None

        try:
            return int(number_string)
        except ValueError:
            assert number_string in {'A', 'R', 'G', '.'}, \
                'incorrect special character value in the Number field'
            return number_string

    return (parse_number(common.find_alist(meta.value, 'Number'))
            if isinstance(meta.value, list) else None)


def field_type(meta: Meta) -> Optional[str]:
    """Return the type field of a structured VCF meta-information line.

    :param meta: A parsed VCF meta-information line.

    :returns: The Type field value if `meta` is structured and
      contains the field of that kind, `None` otherwise.

    """
    def check_type_value(value: Optional[str]) -> Optional[str]:
        """Check if the field type value is correct.

        The check is implemented as an assertion.

        """
        if value is not None:
            assert (value in {'Integer', 'Float', 'Flag', 'Character',
                              'String'}), \
                'incorrect value in the Type field'

        return value

    return (check_type_value(common.find_alist(meta.value, 'Type'))
            if isinstance(meta.value, list) else None)


def to_string(meta: Meta) -> str:
    """Convert a parsed VCF meta-information line to a string.

    :param meta: A parsed VCF meta-information line.

    :returns: The string representation of `meta` in the VCF format.

    """
    if isinstance(meta.value, list):
        value = ('<' + str.join(',', (str.format('{:s}={:s}', j, k)
                                      for j, k in meta.value)) + '>')
    else:
        value = meta.value

    return str.format('##{:s}={:s}', meta.key, value)


def is_meta_line(line: str) -> bool:
    """Is the given line a VCF meta-information line?

    :param line: A line from a VCF file.

    :returns: Is the line a VCF meta-information line?

    """
    return str.startswith(line, '##') and '=' in line
