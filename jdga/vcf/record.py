#!/usr/bin/env python3
"""Routines to process variants records in the VCF format."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import Generator, List, NamedTuple, Optional, Union

_MISSING = '.'


class Genotypes(NamedTuple):
    """FORMAT specification and genotypes from a VCF file."""
    fields: List[str]
    samples: List[List[str]]


class Record(NamedTuple):
    """Variant record from a VCF file."""
    chrom: str
    pos: int
    var_id: List[str]
    ref: str
    alt: List[str]
    qual: Optional[float]
    filters: List[str]
    info: List[str]
    genotypes: Optional[Genotypes]


def of_string(line: str) -> Record:
    """Parse a VCF variant record line.

    :param line: A variant record line from a VCF file.

    :returns: The parsed VCF variant record.

    """
    parts = str.split(str.rstrip(line), '\t', 9)
    assert len(parts) in {8, 10}, 'the number of columns must be 8 or 10'

    def to_list(value: str, sep: str) -> List[str]:
        """Helper function for parsing list-like fields of a VCF record."""
        return [] if value == _MISSING else str.split(value, sep)

    var_gt = None if len(parts) == 8 else Genotypes(str.split(
        parts[8], ':'), [str.split(k, ':') for k in str.split(parts[9], '\t')])

    return Record(parts[0], int(parts[1]), to_list(parts[2], ';'), parts[3],
                  to_list(parts[4], ','),
                  None if parts[5] == _MISSING else float(parts[5]),
                  to_list(parts[6], ';'), to_list(parts[7], ';'), var_gt)


def to_string(record: Record) -> str:
    """Convert a parsed VCF variant record to a string.

    :param record: A parsed VCF variant record.

    :return: The variant record string in the VCF format.

    """
    def of_list(values: Union[List[str], Generator[str, None, None]],
                sep: str) -> str:
        """Helper function for converting a list to a string."""
        return _MISSING if not values else str.join(sep, values)

    locus_part = str.format(
        '{:s}\t{:d}\t{:s}\t{:s}\t{:s}\t{:s}\t{:s}\t{:s}', record.chrom,
        record.pos, of_list(record.var_id, ';'), record.ref,
        of_list(record.alt, ','),
        _MISSING if record.qual is None else str.format('{:g}', record.qual),
        of_list(record.filters, ';'), of_list(record.info, ';'))

    if record.genotypes is None:
        return locus_part

    return str.format(
        '{:s}\t{:s}\t{:s}', locus_part, of_list(record.genotypes.fields, ':'),
        of_list((of_list(k, ':') for k in record.genotypes.samples), '\t'))


def is_snv(record: Record) -> bool:
    """Check if a variant record represents an SNV.

    A variant record is considered an SNV if its reference and
    alternative alleles are single nucleotides.

    :param record: A parsed VCF variant record.

    :returns: Is `record` as SNV?

    """
    return (len(record.ref) == 1 and all(len(k) == 1 for k in record.alt)
            if record.alt else False)


def is_biallelic_snv(record: Record) -> bool:
    """Check if a variant record represents a biallelic SNV.

    This function provides faster implementation than
    :py:func:`is_snv`.

    :param record: A parsed VCF variant record.

    :returns: Is `record` a biallelic SNV?

    """
    return (len(record.ref) == len(record.alt) == len(record.alt[0]) == 1
            if record.alt else False)
