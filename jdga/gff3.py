#!/usr/bin/env python3
"""Reading annotated genomic features from an NCBI GFF3 file.

The module was designed for reading GFF3 files produced by NCBI.

"""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from typing import Dict, List, NamedTuple, Optional, Tuple


class Record(NamedTuple):
    """Record from a GFF3 file."""
    seqid: str
    source: str
    feature_type: str
    start: int
    end: int
    score: Optional[float]
    strand: Optional[str]
    phase: Optional[int]
    attributes: Dict[str, str]


def split2(string: str, sep: str) -> Tuple[str, str]:
    """Split a string into two parts at the specified separator.

    :param line: A string to be split.

    :param sep: A separator at which the string is split.

    :returns: Two parts of the split string.

    """
    sep_pos = str.index(string, sep)
    return (string[:sep_pos], string[sep_pos + 1:])


def parse_attributes(attr_line: str) -> Dict[str, str]:
    """Parse attributes of a GFF3 record.

    :param attr_line: A line from the attributes column of a GFF3
      line.

    :returns: The dictionary which maps names of attributes to their
      values.

    """
    return dict(split2(attr, '=') for attr in str.split(attr_line, ';'))


def parse_line(line: str) -> Record:
    """Parse a record line from a GFF3 file.

    :param line: A line from a GFF3 file.

    :returns: The GFF3 record obtained by parsing `line`.

    """
    line_parts = str.split(str.rstrip(line), '\t', 8)
    return Record(line_parts[0], line_parts[1], line_parts[2],
                  int(line_parts[3]), int(line_parts[4]),
                  None if line_parts[5] == '.' else float(line_parts[5]),
                  None if line_parts[6] == '.' else line_parts[6],
                  None if line_parts[7] == '.' else int(line_parts[7]),
                  parse_attributes(line_parts[8]))


def read(path: str) -> List[Record]:
    """Read records from a GFF3 file.

    :note: The function produces a side effect by reading from file
      `path`.

    :param path: A GFF3 file name.

    :returns: Records of genomic features from the file.

    """
    with open(path) as gff3_file:
        return [
            parse_line(k)
            for k in filter(lambda x: not str.startswith(x, '#'), gff3_file)
        ]


def parse_dbxref(dbxref_value: str) -> List[Tuple[str, str]]:
    """Parse the Dbxref attribute of an NCBI GFF3 record.

    :param dbxref_value: The value of the Dbxref attribute.

    :returns: The association list of database names and IDs in the
      databases.

    """
    return [split2(k, ':') for k in str.split(dbxref_value, ',')]


def is_chromosome(record: Record) -> bool:
    """Check if a record from an NCBI GFF3 file represents a chromosome.

    :param record: A record from an NCBI GFF3 file.

    :returns: Does `record` describe a chromosome?

    """
    return (record.feature_type == 'region' and 'genome' in record.attributes
            and record.attributes['genome'] == 'chromosome')


def chromosome_seqids(records: List[Record]) -> List[Tuple[str, str]]:
    """Get the association list of chromosome sequence IDs and names.

    :param records: Records from an NCBI GFF3 file.

    :returns: The association list of chromosome sequence IDs and
      names.

    """
    return [(k.seqid, k.attributes['chromosome']) for k in records
            if is_chromosome(k)]


Region = Tuple[str, int, int, str]


def gene_coords(records: List[Record]) -> List[Tuple[str, Region]]:
    """Get coordinates of genes from NCBI GFF3 annotation records.

    :param records: Records from an NCBI GFF3 file.

    :returns: The association list of NCBI Gene IDs and coordinates of
      the genes.

    """
    return [(dict(parse_dbxref(k.attributes['Dbxref']))['GeneID'],
             (k.seqid, k.start, k.end, k.attributes['Name'])) for k in records
            if k.feature_type == 'gene' and 'Dbxref' in k.attributes]
