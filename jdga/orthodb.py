#!/usr/bin/env python3
"""Routines for working with the OrthoDB database."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import gzip
import os.path
from typing import Callable, List, NamedTuple, Optional, Set, Tuple


def load_og_genes(root_path: str, level_id: str) -> List[Tuple[str, str]]:
    """Return pairs of OrthoDB orthologous groups IDs and OrthoDB gene ID.

    :note: The function produces a side effect by reading from a file.

    :param root_path: The OrthoDB dump root directory.

    :param level_id: OrthoDB level ID that specifies orthologous
      groups (OGs) to be returned.

    :returns: The list of OG and OrthoDB ID pairs for the specified
      `level_id`.

    """
    og_path = os.path.join(root_path, 'odb10v0_OG2genes.tab.gz')
    og_genes: List[Tuple[str, str]] = []

    def parse_og_line(line: str) -> Tuple[str, str, str]:
        """Parse a line from file 'odbv10_OG2genes.tab.gz'."""
        og_id, gene_id = str.split(str.rstrip(line), '\t', 1)
        og_level_id = str.split(og_id, 'at', 1)[1]
        return (og_id, og_level_id, gene_id)

    with gzip.open(og_path, 'rt') as og_file:
        for line in og_file:
            og_id, og_level_id, gene_id = parse_og_line(line)
            if og_level_id == level_id:
                list.append(og_genes, (og_id, gene_id))

    return og_genes


class Gene(NamedTuple):
    """Gene record as in file `odb10v0_genes.tab.gz` of the OrthoDB dump."""
    orthodb_id: str  #: OrthoDB unique gene ID
    organism_tax_id: str  #: Taxonomy ID of an organism
    protein_seq_id: str  #: Original protein sequence ID
    uniprot_id: Optional[str]
    ensembl_gene_name: Optional[str]
    ncbi_gene_id: Optional[str]
    description: Optional[str]


def load_genes(root_path: str, orthodb_ids: Set[str]) -> List[Gene]:
    """Load gene records from the OrthoDB dump for the specified genes.

    :note: The function produces a side effect by reading from a file.

    :param root_path: The OrthoDB dump root directory.

    :param orthodb_ids: OrthoDB IDs for genes which records to load.

    :returns: OrthoDB gene records for genes with IDs from `orthodb_ids`.

    """
    def optional(value: str) -> Optional[str]:
        """Parse a missing value."""
        return value if value != '' else None

    OptStr = Optional[str]

    def parse_ids(line_parts: List[str]) -> Tuple[OptStr, OptStr, OptStr]:
        """Parse optional ID fields of a line."""
        parsed = [optional(k) for k in line_parts]
        if len(parsed) < 3:
            parsed += [None] * (3 - len(parsed))
        return (parsed[0], parsed[1], parsed[2])

    def parse_line(line: str) -> Gene:
        """Parse a line from file `odb10v0_genes.tab.gz`."""
        line_parts = str.split(str.rstrip(line), '\t', 6)

        id_part = parse_ids(line_parts[3:6])
        description = None if len(line_parts) < 7 else line_parts[6]
        return Gene(line_parts[0], line_parts[1], line_parts[2], *id_part,
                    description)

    genes: List[Gene] = []

    gene_path = os.path.join(root_path, 'odb10v0_genes.tab.gz')
    with gzip.open(gene_path, 'rt') as gene_file:
        for line in gene_file:
            record = parse_line(line)
            if record.orthodb_id in orthodb_ids:
                list.append(genes, record)

    return genes


def og_ncbi_ids(og_genes: List[Tuple[str, str]],
                genes: List[Gene]) -> Callable[[str], List[Tuple[str, str]]]:
    """Return a function that creates the OG-NCBI Gene ID association list.

    :param og_genes: OG and OrthoDB ID pairs as returned by
      :py:func:`load_og_genes`.

    :param genes: OrthoDB gene records as returned by
      :py:func:`load_genes`.

    :returns: The functions that takes the taxonomy ID of a species
      and returns the association list of OGs and NCBI Gene IDs for
      the species.

    """
    def create_assoc_list(tax_id: str) -> List[Tuple[str, str]]:
        """Function to be returned."""
        gene_id_dict = {
            k.orthodb_id: k.ncbi_gene_id
            for k in genes
            if k.organism_tax_id == tax_id and k.ncbi_gene_id is not None
        }
        return [(og_id, gene_id_dict[orthodb_id])
                for og_id, orthodb_id in og_genes
                if orthodb_id in gene_id_dict]

    return create_assoc_list


def load_og_ncbi_geneids(root_path: str, level_id: str,
                         species_id: str) -> List[Tuple[str, str]]:
    """Return the association list of OrthoDB IDs and NCBI Gene IDs.

    :note: The function produces side effects by opening files from
      the OrthoDB dump for reading.  These side effects are caused by
      calling functions :py:func:`load_og_genes` and
      :py:func:`load_genes`.

    :param root_path: The OrthoDB dump root directory.

    :param level_id: The OrthoDB level ID as in function
      :py:func:`load_og_genes`.

    :param species_id: The taxonomy ID as passed to the function
      returned by :py:func:`og_ncbi_ids`.

    :returns: The association list of OrthoDB IDs of genes and their
      NCBI Gene IDs.

    """
    og_genes = load_og_genes(root_path, level_id)
    gene_id_fn = og_ncbi_ids(
        og_genes, load_genes(root_path, set(k[1] for k in og_genes)))
    return gene_id_fn(species_id)
