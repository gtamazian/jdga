#!/usr/bin/env python3
"""Routines to process nucleotide sequences.

We assume that sequences are represented by `bytes`.

"""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import itertools
import functools
from typing import Iterator, List, Tuple, Union

_NUCL_A = ord(b'A')
_NUCL_C = ord(b'C')
_NUCL_G = ord(b'G')
_NUCL_T = ord(b'T')
_NUCL_N = ord(b'N')


def complement(nucleotide: int) -> int:
    """Return a complement nucleotide character.

    :param nucleotide: A nucleotide character.

    :returns: The complement nucleotide character.

    """
    if nucleotide == _NUCL_A:
        return _NUCL_T

    if nucleotide == _NUCL_C:
        return _NUCL_G

    if nucleotide == _NUCL_G:
        return _NUCL_C

    if nucleotide == _NUCL_T:
        return _NUCL_A

    return _NUCL_N


def rev_comp(sequence: bytes) -> bytes:
    """Get reverse complement of a nucleotide sequence.

    :param sequence: A nucleotide sequence.

    :returns: The reverse complement sequence.

    """
    return bytes(complement(k) for k in reversed(sequence))


def is_gap(nucleotide: int) -> bool:
    """Check if a nucleotide character represents a gap.

    :param nucleotide: A nucleotide character.

    :returns: Is the character a gap?

    """
    return nucleotide == _NUCL_N


def is_gap_sequence(sequence: bytes) -> bool:
    """Check if a nucleotide gap contains only gaps (Ns).

    The Supernova assembler may produce scaffolds that contain only
    gaps. This function can be used to detect such scaffolds.

    :param sequence: A nucleotide sequence.

    :returns: Does the sequence contain only gaps?

    """
    return all(k == _NUCL_N for k in sequence)


def gap_prefix_len(sequence: Iterator[int]) -> int:

    """Return the length of the gap prefix for a sequence.

    I specified the type for the function argument as `Iterator[int]`
    to be able to implement :py:func:`gap_suffix_len` as a call of
    this function for the reversed sequence.

    :param sequence: A nucleotide sequence.

    :returns: The length of the gap prefix for the sequence.

    """
    if not sequence:
        return 0

    prefix_length = 0
    for prefix_length, k in enumerate(sequence):
        if not is_gap(k):
            return prefix_length

    return prefix_length + 1


def gap_suffix_len(sequence: bytes) -> int:
    """Return the length of the gap suffix for a sequence.

    :param sequence: A nucleotide sequence.

    :returns: The length of the gap suffix for the sequence.

    """
    return gap_prefix_len(reversed(sequence))


Part = Union[bytes, int]


def to_contigs(sequence: bytes) -> List[Part]:
    """Split a sequence into contigs and gaps.

    In the output list, gaps are presented by integers and contigs are
    represented by `bytes`.

    :param sequence: A nucleotide sequence.

    :returns: The list of contig sequences and gap lengths.

    """

    Acc = Tuple[Union[List[int], int], List[Part]]

    def process_nucleotide(acc: Acc, nucleotide: int) -> Acc:
        """Function passed to functools.reduce."""
        current, contigs = acc
        if is_gap(nucleotide):
            if isinstance(current, int):
                current += 1
            else:
                list.append(contigs, bytes(current))
                current = 1
        else:
            if isinstance(current, int):
                list.append(contigs, current)
                current = [nucleotide]
            else:
                list.append(current, nucleotide)

        return (current, contigs)

    if not sequence:
        return []

    init: Acc = (1 if is_gap(sequence[0]) else [sequence[0]], [])
    rest = sequence[1:]

    last, contigs = functools.reduce(process_nucleotide, rest, init)

    if isinstance(last, int):
        list.append(contigs, last)
    elif last:
        list.append(contigs, bytes(last))

    return contigs


def of_contigs(contigs: List[Part]) -> bytes:
    """Merge a list of contigs with gaps into a single sequence.

    :param contigs: A list of contigs and gaps as returned by function
      :py:func:`to_contigs`.

    :returns: The sequence obtained by merging the contigs and the gaps.

    """
    return bytes(
        itertools.chain.from_iterable(b'N' * k if isinstance(k, int) else k
                                      for k in contigs))


Region = Tuple[int, int]


def gap_regions(sequence: bytes) -> List[Region]:
    """Return the list of gap ranges in a sequence.

    The ranges are zero-based and half-open as in the BED format.

    :param sequence: A nucleotide sequence.

    :returns: Ranges of gaps in `sequence`.

    """
    if not sequence:
        return []

    first, rest = sequence[0], sequence[1:]

    Acc = Tuple[Union[int, None], List[Region]]

    init: Acc = (0, []) if is_gap(first) else (None, [])

    def process_nucleotide(acc: Acc, new: Tuple[int, int]) -> Acc:
        """Function passed to functools.reduce.

        The second element of parameter `new` actually is `bytes` but
        I have to specify type `int` because iterating a `bytes`
        variable gives `int` values.  The value is converted from
        `int` to `bytes` in the beginning of the function.

        """
        current, gaps = acc
        pos, nucleotide = new

        if is_gap(nucleotide):
            if current is None:
                # a gap started, keep its start position
                current = pos
        else:
            if current is not None:
                # we got out of a gap, add a new gap range
                list.append(gaps, (current, pos))
                current = None

        return (current, gaps)

    last, gaps = functools.reduce(process_nucleotide, enumerate(rest, 1), init)

    if last is not None:
        list.append(gaps, (last, len(sequence)))

    return gaps


def split_by_gaps(sequence: bytes, min_gap: int) -> List[bytes]:
    """Split a sequence by gaps longer than the specified threshold.

    :param sequence: A nucleotide sequence.

    :param min_gap: The minimal length of gaps at which the sequence
      is split.

    :returns: The nucleotide sequence fragments that does not contain
      gaps longer than `min_gap`.

    """
    Acc = Tuple[List[bytes], List[Part]]

    def process_part(acc: Acc, new_part: Part) -> Acc:
        """Helper function passed to functools.reduce."""
        fragments, current = acc
        if isinstance(new_part, int):
            if new_part < min_gap:
                list.append(current, new_part)
            else:
                list.append(fragments, of_contigs(current))
                current = []
        else:
            list.append(current, new_part)

        return (fragments, current)

    init: Acc = ([], [])
    seq_fragments, last = functools.reduce(process_part, to_contigs(sequence),
                                           init)
    if last:
        list.append(seq_fragments, of_contigs(last))

    return seq_fragments


def regions_by_gaps(sequence: bytes, min_gap: int) -> List[Region]:
    """Get regions without gaps longer than the threshold.

    This functions returns regions of sequence parts that would be
    produced by function :py:func:`split_by_gaps` with the same
    threshold.

    :param sequence: A nucleotide sequence.

    :param min_gap: The minimal length of gaps which form the regions.

    :returns: Regions in `sequence` without gaps longer than
      `min_gap`.

    """
    # We use that function gap_regions returns sorted regions that do
    # not overlap.
    seq_gaps = [k for k in gap_regions(sequence) if k[1] - k[0] >= min_gap]

    if not seq_gaps:
        return [(0, len(sequence))]

    Acc = Tuple[List[Region], int]

    def process_gap(acc: Acc, new_gap: Region) -> Acc:
        """Helper function to be passed to functools.reduce."""
        gaps, cur_pos = acc
        if cur_pos < new_gap[0]:
            list.append(gaps, (cur_pos, new_gap[0]))
        return (gaps, new_gap[1])

    init: Acc = ([], 0)
    regions, last_pos = functools.reduce(process_gap, seq_gaps, init)

    if last_pos < len(sequence):
        list.append(regions, (last_pos, len(sequence)))

    return regions
