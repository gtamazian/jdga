#!/usr/bin/env python3
"""Routines to read and write FASTA files.

We assume that sequences and names of FASTA records are stored as
`bytes`.

"""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import functools
import gzip
import itertools
import operator
from typing import IO, List, NamedTuple, Optional, Tuple


class Record(NamedTuple):
    """Named sequence record from a FASTA file."""
    name: bytes
    seq: bytes


def is_gzipped(path: str) -> bool:
    """Check if a file is compressed with gzip.

    The function opens the specified file to check if it starts with
    magic bytes specific to a gzip-compressed file.

    :note: The function produces a side effect by opening file `path`
      for reading.

    :param path: The name of a file.

    :returns: Is the specified file gzip-compressed?

    """
    with open(path, 'rb') as path_file:
        return path_file.read(2) == b'\x1f\x8b'


def read(path: str, gzipped: Optional[bool] = None) -> List[Record]:
    """Read sequence records from a FASTA file.

    Both uncompressed and gzipped FASTA files are supported.  By
    default, the function determines if the input file is
    gzip-compressed by calling function :py:func:`is_gzipped`, which
    checks the first two bytes of the file.  To avoid an extra opening
    of the file, a user may specify whether the input file is gzipped
    by setting parameter `gzipped`.

    :note: The function produces a side effect by opening file *path*
      for reading.

    :param path: The name of the FASTA file.

    :param gzipped: Is the FASTA file gzip-compressed?

    :returns: The list of sequence records from the FASTA file.

    """
    if gzipped is None:
        # determine whether the specified file is gzip-compressed
        gzipped = is_gzipped(path)

    def read_rstripped_lines(stream: IO[bytes]) -> List[bytes]:
        """Read lines without trailing white spaces from the stream."""
        return [bytes.strip(line) for line in stream]

    if gzipped:
        with gzip.open(path, 'rb') as fasta_file:
            lines = read_rstripped_lines(fasta_file)
    else:
        with open(path, 'rb') as fasta_file:
            lines = read_rstripped_lines(fasta_file)

    if not lines:
        return []

    def strip_header(header_line: bytes) -> bytes:
        """Remove symbol '>' from the beginning of a FASTA header line."""
        assert header_line[:1] == b'>', 'a header line must start with ">"'
        return header_line[1:]

    Acc = Tuple[List[bytes], List[Record]]

    def process_line(records: Acc, new_line: bytes) -> Acc:
        """Helper function passed to functools.reduce."""
        current, complete = records
        if bytes.startswith(new_line, b'>'):
            new_record = Record(strip_header(current[0]),
                                bytes.join(b'', current[1:]))
            list.append(complete, new_record)
            current = [new_line]
        else:
            list.append(current, new_line)

        return (current, complete)

    assert bytes.startswith(lines[0], b'>'), \
        'the first line must be a record header'

    init: Acc = ([lines[0]], [])
    last_rec, records = functools.reduce(process_line, lines[1:], init)

    list.append(
        records,
        Record(strip_header(last_rec[0]), bytes.join(b'', last_rec[1:])))

    return records


def write(records: List[Record],
          path: str,
          gzipped: bool = False,
          wrapped: Optional[int] = None) -> None:
    """Write sequence records to a FASTA file.

    :note: The function produces a side effect by opening file *path*
      for writing.

    :param records: Sequence records to be written to the FASTA file.

    :param path: The name of the FASTA file.

    :param gzipped: Will the FASTA file be gzip-compressed?

    :param wrapped: Wrap sequence lines to the specified length.

    """
    def wrap(sequence: bytes, width: int) -> List[bytes]:
        """Wrap a bytes sequence like `textwrap.wrap`.

        :param sequence: A sequence to wrap.

        :param width: The maximum length of an output sequence.

        :returns: The list of sequences which length is not greater
          than `width`.
        """
        if not sequence:
            return []

        assert width > 0, 'width must be a positive number'
        return [
            sequence[k:(k + width)] for k in itertools.takewhile(
                lambda x: x < len(sequence), itertools.count(0, width))
        ]

    def write_records_to_stream(stream: IO[bytes]) -> None:
        """Write records in the FASTA format to the stream."""
        for name, seq in records:
            stream.write(b'>' + name + b'\n')
            if wrapped is not None:
                stream.write(bytes.join(b'\n', wrap(seq, wrapped)))
            else:
                stream.write(seq)
            stream.write(b'\n')

    if gzipped:
        with gzip.open(path, 'wb') as fasta_file:
            write_records_to_stream(fasta_file)
    else:
        with open(path, 'wb') as fasta_file:
            write_records_to_stream(fasta_file)


def remove_soft_mask(record: Record) -> Record:
    """Remove soft mask from the sequence of a record.

    Routines from module :py:mod:`sequence` require records to have
    unmasked sequences.

    :param record: A sequence record.

    :returns: The record which sequence does not have soft-masked
      (that is, lower-case) regions.

    """
    name, seq = record
    return Record(name, bytes.upper(seq))


def remove_comment(record: Record) -> Record:
    """Remove a comment from the name of a sequence record.

    For some applications, comments in record names are not needed.
    For example, it is more convenient to process various features of
    sequences and being able to identify them using their record
    names.  Comments usually are not used for identifying the records.

    :param record: A sequence record.

    :returns: The record which name does not a comment.

    """
    name, seq = record
    return Record(bytes.split(name, maxsplit=1)[0], seq)


def duplicates(records: List[Record]) -> List[List[bytes]]:
    """Detect records that have identical sequences.

    Soft masks are removed when comparing record sequences.  Sequence
    names in the returned result are given without comments.

    :param records: Sequence records.

    :returns: Lists of names of records that have identical sequences.
      The names are given without comments.

    """
    def sort_by_seq(records: List[Record]) -> List[Record]:
        """Sort records by their sequences with soft mask removed."""
        return sorted((remove_soft_mask(k) for k in records),
                      key=operator.attrgetter('seq'))

    rec_names = [[remove_comment(k).name for k in seq_records]
                 for _, seq_records in itertools.groupby(
                     sort_by_seq(records), key=operator.attrgetter('seq'))]
    return [k for k in rec_names if len(k) > 1]
