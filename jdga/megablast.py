#!/usr/bin/env python3
"""Routines for working with NCBI MegaBLAST output."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

from enum import Enum, auto
from typing import List, NamedTuple


class Direction(Enum):
    """Direction of a region on a sequence."""
    FORWARD = auto()
    REVERSE = auto()


class Region(NamedTuple):
    """Sequence region."""
    seq: str
    start: int
    end: int
    direction: Direction


def region_of_string(seq: str, start: str, end: str) -> Region:
    """Create a sequence record.

    :param seq: The region sequence.

    :param start: The start position on the sequence.

    :param end: The end position on the sequence.

    :returns: The directed region which start position is always less
      or equal to the end position.

    """
    start_int, end_int = int(start), int(end)
    return Region(
        seq, min(start_int, end_int), max(start_int, end_int),
        Direction.FORWARD if int(start) < int(end) else Direction.REVERSE)


def is_reverse(region: Region) -> bool:
    """Check if the region direction is reverse.

    :param region: A region on a sequence.

    :returns: Is `region` oriented in the reverse direction?

    """
    return region.direction == Direction.REVERSE


class Alignment(NamedTuple):
    """NCBI MegaBLAST alignment record."""
    query: Region
    subject: Region
    perc_identity: float
    alignment_len: int
    mismatches: int
    gap_openings: int
    e_value: float
    bit_score: float


def of_string(line: str) -> Alignment:
    """Parse a MegaBLAST output line.

    :param line: A line from an NCBI MegaBLAST file.

    :returns: The parsed alignment record.

    """
    parts = str.split(line, '\t', 11)
    assert len(parts) == 12
    return Alignment(region_of_string(parts[0], parts[6], parts[7]),
                     region_of_string(parts[1], parts[8], parts[9]),
                     float(parts[2]), int(parts[3]), int(parts[4]),
                     int(parts[5]), float(parts[10]), float(parts[11]))


def to_string(alignment: Alignment) -> str:
    """Convert a parsed alignment to a string.

    :param alignment: A parsed alignment record.

    :returns: The alignment line in the NCBI MegaBLAST format.

    """
    return str.format(
        '{:s}\t{:s}\t{:.2f}\t{:d}\t{:d}\t{:d}'
        '\t{:d}\t{:d}\t{:d}\t{:d}\t{:g}\t{:g}', alignment.query.seq,
        alignment.subject.seq, alignment.perc_identity,
        alignment.alignment_len, alignment.mismatches, alignment.gap_openings,
        alignment.query.start, alignment.query.end,
        (alignment.subject.end
         if is_reverse(alignment.subject) else alignment.subject.start),
        (alignment.subject.start if is_reverse(alignment.subject) else
         alignment.subject.end), alignment.e_value, alignment.bit_score)


def read(path: str) -> List[Alignment]:
    """Read alignments from a MegaBLAST output file.

    :note: The function produces side effects by reading from file
      `path`.

    :param path: The NCBI MegaBLAST output file.

    :returns: The list of alignments from the file.

    """
    with open(path) as alignment_file:
        return [
            of_string(line) for line in filter(
                lambda x: not str.startswith(x, '#'), alignment_file)
        ]
