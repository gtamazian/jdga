#!/usr/bin/env python3
"""Routines related to chromosome colors."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import itertools
import math
from typing import List, NamedTuple, Tuple

import matplotlib.patches as patches
import matplotlib.pyplot as plt

# The following colors were taken from Circos's file
# colors.ucsc.conf. Circos refers to these colors as the "default UCSC
# color scheme for chromosome colors.

_UCSC_COLORS = [(153, 102, 0), (102, 102, 0), (153, 153, 30), (204, 0, 0),
                (255, 0, 0), (255, 0, 204), (255, 204, 204), (255, 153, 0),
                (255, 204, 0), (255, 255, 0), (204, 255, 0), (0, 255, 0),
                (53, 128, 0), (0, 0, 204), (102, 153, 255), (153, 204, 255),
                (0, 255, 255), (204, 255, 255), (153, 0, 204), (204, 51, 255),
                (204, 153, 255), (102, 102, 102), (153, 153, 153),
                (204, 204, 204), (204, 204, 153), (121, 204, 61)]

RGB = Tuple[float, float, float]


def ucsc_colors() -> List[RGB]:
    """Return RGB colors from the default UCSC color scheme.

    :returns: The list of colors in the RGB format.

    """
    def normalize(color: Tuple[int, int, int]) -> RGB:
        """Helper function to normalize color values."""
        red, green, blue = color
        return (red / 255, green / 255, blue / 255)

    return [normalize(k) for k in _UCSC_COLORS]


def is_dark(color: RGB) -> bool:
    """Check if the given color is too dark.

    :returns: Is `color` dark?

    """
    red, green, blue = color
    return red + green + blue < 1


class FillStyle(NamedTuple):
    """Style for filling genome diagram blocks."""
    color: RGB
    hatch: int


def fill_styles(labels: List[str]) -> List[Tuple[str, FillStyle]]:
    """Assign filling styles to specified labels.

    The order of colors in the returned styles is specified by function
    :py:func:`ucsc_colors`.

    :param labels: Chromosome or sequence names.

    :returns: The association list of the names and their filling
      styles.

    """
    return list(
        zip(
            labels,
            itertools.chain.from_iterable(
                map(lambda x: [FillStyle(k, x) for k in ucsc_colors()],
                    itertools.count(0)))))


def legend(labels: List[str], fig_size: int, font_size: int) -> plt.Figure:
    """Produce a chromosome color legend figure.

    :param labels: Chromosome names.

    :param fig_size: Figure size in data coordinates.

    :param font_size: Label font size in points.

    :returns: The Matplotlib figure object that represents the
      chromosome color legend.

    """
    fig, axes = plt.subplots()
    plt.Axes.axis(axes, 'off')
    plt.Axes.set_xlim(axes, 0, fig_size)
    plt.Axes.set_ylim(axes, 0, fig_size)
    plt.Axes.invert_yaxis(axes)

    num_side = math.ceil(math.sqrt(len(labels)))
    alpha = .1

    def block_width(alpha: float) -> float:
        """Helper function to get the width between the blocks."""
        return fig_size / (num_side + num_side * alpha + alpha + .5)

    width = block_width(alpha)

    def draw_block(x_start: float, y_start: float, label: str, color: RGB,
                   hatch: int) -> None:
        """Helper function to draw a chromosome block."""
        plt.Axes.add_patch(
            axes,
            patches.Rectangle((x_start, y_start),
                              width=width,
                              height=width,
                              color=color,
                              linewidth=0))
        plt.Axes.text(axes,
                      x_start + alpha + width / 2,
                      y_start + alpha + width / 2,
                      label,
                      horizontalalignment='center',
                      verticalalignment='center_baseline',
                      fontsize=font_size,
                      color='white' if is_dark(color) else 'black')
        if hatch > 0:
            plt.Axes.add_patch(
                axes,
                patches.Rectangle(
                    (x_start, y_start),
                    width=width,
                    height=width,
                    fill=False,
                    hatch=('//' * hatch),
                    edgecolor='white' if is_dark(color) else 'black',
                    linewidth=1))
        plt.Axes.add_patch(
            axes,
            patches.Rectangle((x_start, y_start),
                              width=width,
                              height=width,
                              fill=False,
                              edgecolor='black',
                              linewidth=1))

    for k, (name, style) in zip(itertools.product(range(num_side), repeat=2),
                                fill_styles(labels)):
        draw_block(alpha + width / 4 + k[1] * (1 + alpha) * width,
                   alpha + width / 4 + k[0] * (1 + alpha) * width, name,
                   style.color, style.hatch)

    return fig
