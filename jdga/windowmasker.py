#!/usr/bin/env python3
"""Functions to process WindowMasker output files."""

__author__ = 'Gaik Tamazian'
__copyright__ = 'Copyright 2021, Gaik Tamazian'
__email__ = 'mail@gtamazian.com'
__license__ = 'GPLv3'

import functools
from typing import List, Tuple

Range = Tuple[bytes, int, int]


def read_intervals(path: str) -> List[Tuple[bytes, int, int]]:
    """Read masked regions from a WindowMasker interval file.

    :note: This function produces a side effect by reading from file
      `path`.

    :param path: A WindowMasker interval file.

    :returns: The list of masked regions from the interval file.

    """
    with open(path, 'rb') as interval_file:
        lines = [bytes.rstrip(k) for k in interval_file]

    if not lines:
        return []

    def is_header(line: bytes) -> bool:
        """Does the line start a new sequence section?"""
        return bytes.startswith(line, b'>')

    def strip_header(line: bytes) -> bytes:
        """Remove the starting '>' character from a sequence header."""
        assert line[:1] == b'>', 'a header line must start with ">"'
        return bytes.split(line[1:], None, 1)[0]

    def get_range(line: bytes) -> Tuple[int, int]:
        """Parse an interval line."""
        pos_s, pos_e = bytes.split(line, b' - ', 1)
        return (int(pos_s), int(pos_e))

    Acc = Tuple[List[Range], bytes]

    def process_line(acc: Acc, new_line: bytes) -> Acc:
        """Helper function to be passed to functools.reduce."""
        ranges, cur_seq = acc
        if is_header(new_line):
            cur_seq = strip_header(new_line)
        else:
            list.append(ranges, (cur_seq, ) + get_range(new_line))
        return (ranges, cur_seq)

    init: Acc = ([], strip_header(lines[0]))
    masked_regions, _ = functools.reduce(process_line, lines[1:], init)

    return masked_regions


def write_bed(ranges: List[Range], path: str) -> None:
    """Write ranges to a file in the BED format.

    :note: The function produces a side effect by writing to file
      `path`.

    :param ranges: A list of ranges as returned by function
      :py:func:`read_intervals`.

    :param path: An output file name.

    """
    with open(path, 'wb') as bed_file:
        for seq, pos_s, pos_e in ranges:
            bed_file.write(b'%b\t%d\t%d\n' % (seq, pos_s, pos_e))
