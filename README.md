JDGA
====

This package provides routines used in the data analysis for paper

Gaik Tamazian, Pavel Dobrynin, Anna Zhuk, Daria V Zhernakova, Polina L
Perelman, Natalia A Serdyukova, Alexander S Graphodatsky, Aleksey
Komissarov, Sergei Kliver, Nikolay Cherkasov, Alan F Scott, David W
Mohr, Klaus-Peter Koepfli, Stephen J O’Brien, Ksenia Krasheninnikova,
Draft *de novo* Genome Assembly of the Elusive Jaguarundi, *Puma
yagouaroundi*, *Journal of Heredity*, 2021.

https://doi.org/10.1093/jhered/esab036

# Installation

The package can be conveniently installed with its dependencies using
**pip**:

```
pip install git+https://gitlab.com/gtamazian/jdga.git
```

# Documentation

Source code of the package is extensively documented using
documentation strings
(docstrings). [Sphinx](https://www.sphinx-doc.org) is recommended to
generate documentation from the docstrings.

# Using the package

Examples for using the package are given in two
[Snakemake](https://snakemake.github.io/) pipelines that accompany the
jaguarundi draft genome assembly paper. The pipelines with their
auxiliary scripts are available in the separate [GitLab
repository](https://gitlab.com/gtamazian/jdga_pipelines).
