#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name='jdga',
    version='0.1.0',
    license='GPLv3',
    description='Software package paper "Jaguarundi Draft Genome Assembly"',
    author='Gaik Tamazian',
    author_email='mail@gtamazian.com',
    packages=setuptools.find_packages(),
    package_data={'jdga': ['py.typed']},
    python_requires='>=3.6.9',
    install_requires=['matplotlib'],
    zip_safe=False,
)
